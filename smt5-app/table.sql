CREATE DATABASE smt5app;

USE smt5app;

CREATE TABLE app_users (
    id bigint unsigned primary key auto_increment,
    uuid VARCHAR(100) default (uuid()) not null unique,
    long_name varchar(100) not null,
    username varchar(50) not null,
    password varchar(256) not null,
    profile_pict_path varchar(100) not null,
    cover_pict_path varchar(100) not null,
    email varchar(100) not null,
    phone_number varchar(100) not null default '',
    activated_at datetime null,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null
);

CREATE TABLE user_posts (
    id bigint unsigned primary key auto_increment,
    uuid VARCHAR(100) not null DEFAULT (uuid()) unique,
    user_uuid VARCHAR(100) not null,
    text_content text,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null,
	CONSTRAINT FK_UserPost_UserUUID FOREIGN KEY (user_uuid) REFERENCES app_users(uuid)
);

CREATE TABLE user_post_files (
    id bigint unsigned primary key auto_increment,
    post_uuid VARCHAR(100) not null,
    file_path varchar(256) not null,
    file_name varchar(100) not null,
    file_type varchar(50) not null,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null,
	CONSTRAINT FK_UserPostFiles_PostUUID FOREIGN KEY (post_uuid) REFERENCES user_posts(uuid)
);

CREATE TABLE post_comments (
    id bigint unsigned primary key auto_increment,
    post_uuid VARCHAR(100) not null,
    user_uuid VARCHAR(100) not null,
    comment_txt text not null,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null,
	CONSTRAINT FK_PostComment_PostUUID FOREIGN KEY (post_uuid) REFERENCES user_posts(uuid),
	CONSTRAINT FK_PostComment_AppUserUUID FOREIGN KEY (user_uuid) REFERENCES app_users(uuid)
);

CREATE TABLE user_follow (
    id bigint unsigned primary key auto_increment,
    user_uuid VARCHAR(100) not null,
	user_followed_uuid VARCHAR(100) not null,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null,
	CONSTRAINT FK_UserFollow_UserUUID FOREIGN KEY (user_uuid) REFERENCES app_users(uuid),
    CONSTRAINT FK_UserFollow_UserFolowedUUID FOREIGN KEY (user_followed_uuid) REFERENCES app_users(uuid)
);

CREATE TABLE user_direct_messages (
    id bigint unsigned primary key auto_increment,
    sender_user_uuid VARCHAR(100) not null,
    receiver_user_uuid VARCHAR(100) not null,
    message_txt text not null,
    created_at datetime not null default now(),
    updated_at datetime null,
    deleted_at datetime null,
	CONSTRAINT FK_PostComment_SenderUserUUID FOREIGN KEY (sender_user_uuid) REFERENCES app_users(uuid),
	CONSTRAINT FK_PostComment_ReceiverUserUUID FOREIGN KEY (receiver_user_uuid) REFERENCES app_users(uuid)
);