<?php
namespace App\Usecases\Chat;
use App\Dto\AppUserDto;
use App\Dto\DirectMessageDto;
use App\Dto\DirectMessageViewDto;
use App\Entities\UserDirectMessageEntity;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\UserDirectMessageRepository;
use App\Usecases\Chat\Models\GetDirectMessageResponseModel;
use App\Usecases\Chat\Models\GetListDirectMessageResponseModel;
use App\Usecases\Chat\Models\SendMessageResponseModel;
use Exception;

class ChatUsecase{

    /**
     * @var UserDirectMessageRepository 
    */
    private $userDirectMessageRepository;

    /**
     * @var AppUserRepository 
    */
    private $appUserRepository;

    /**
     * @var ChatUsecase
    */
    private static $instance = null;

    public static function getInstance() : ChatUsecase{
        if(self::$instance == null){
            self::$instance = new ChatUsecase(
                UserDirectMessageRepository::getInstance(),
                AppUserRepository::getInstance()
            );
        }

        return self::$instance;
    }

    public function __construct(
        UserDirectMessageRepository $userDirectMessageRepository,
        AppUserRepository $appUserRepository
    ){
        $this->userDirectMessageRepository = $userDirectMessageRepository;
        $this->appUserRepository = $appUserRepository;
    }

    /**
     * @param string[]
     * @return AppUserDto[]
    */
    private function getMapUserDto(array $uuids) : array{
        if(empty($uuids)){
            return [];
        }
        $listUser = $this->appUserRepository->getByListUUID($uuids);

        $s3ExternalHost = getenv('S3_EXTERNAL_ENDPOINT');

        /**
         * @var AppUserDto[]
        */
        $mapUserDtoByUUID = [];
        foreach($listUser as $user){
            $dataDto = new AppUserDto(null, $user);
            $dataDto->profilePictPath = $s3ExternalHost.'/'.$dataDto->profilePictPath;
            $dataDto->coverPictPath = $s3ExternalHost.'/'.$dataDto->coverPictPath;
            $mapUserDtoByUUID[$user->uuid] = $dataDto;
        }

        return $mapUserDtoByUUID;
    }

    public function getDirectMessageData(string $userUUID, string $targetUserUUID) : GetDirectMessageResponseModel{
        $reponse = new GetDirectMessageResponseModel();
        try{
            /**
             * @var AppUserDto[]
            */
            $mapUserDtoByUUID = $this->getMapUserDto([$userUUID,$targetUserUUID]);
            $listMsg = $this->userDirectMessageRepository->getByUserUUID($userUUID,$targetUserUUID);
            
            /**
             * @var DirectMessageDto[]
            */
            $listMsgDto = [];
            foreach($listMsg as $msg){
                $dataDto = new DirectMessageDto();
                $dataDto->id = $msg->id;
                $dataDto->message = $msg->messageTxt;
                $dataDto->createdAt = $msg->createdAt;
                $dataDto->sender = $mapUserDtoByUUID[$msg->senderUserUUID];
                $dataDto->receiver = $mapUserDtoByUUID[$msg->receiverUserUUID];
                array_push($listMsgDto,$dataDto);
            }

            $directMessageData = new DirectMessageViewDto();
            $directMessageData->user = $mapUserDtoByUUID[$userUUID];
            $directMessageData->targetUser = $mapUserDtoByUUID[$targetUserUUID];
            $directMessageData->messages = $listMsgDto;

            $reponse->statusCode = "CHAT000";
            $reponse->message = "success";
            $reponse->data = $directMessageData;

        }catch(Exception $ex){
            $reponse->statusCode = "CHAT500";
            $reponse->message = "Internal server error";
            $reponse->errorMessage = $reponse->message;
            dd($ex);
        }
        return $reponse;
    }

    public function sendDirectMessage(string $senderUUID, string $receiverUUID, string $msg) : SendMessageResponseModel{
        $reponse = new SendMessageResponseModel();
        try{

            $dmEntity = new UserDirectMessageEntity();
            $dmEntity->senderUserUUID = $senderUUID;
            $dmEntity->receiverUserUUID = $receiverUUID;
            $dmEntity->messageTxt = $msg;
            $dmEntity = $this->userDirectMessageRepository->create($dmEntity);

            /**
             * @var AppUserDto[]
            */
            $mapUserDtoByUUID = $this->getMapUserDto([$senderUUID,$receiverUUID]);

            $msgDto = new DirectMessageDto();
            $msgDto->sender = $mapUserDtoByUUID[$senderUUID];
            $msgDto->receiver = $mapUserDtoByUUID[$receiverUUID];
            $msgDto->message = $dmEntity->messageTxt;
            $msgDto->createdAt = $dmEntity->createdAt;

            $reponse->statusCode = "CHAT000";
            $reponse->message = "success";
            $reponse->data = $msgDto;

        }catch(Exception $ex){
            $reponse->statusCode = "CHAT500";
            $reponse->message = "Internal server error";
            $reponse->errorMessage = $reponse->message;
            dd($ex);
        }
        return $reponse;
    } 

    public function getListChatByUserUUID(string $userUUID, int $perpage = 5, int $page = 1) : GetListDirectMessageResponseModel{
        $reponse = new GetListDirectMessageResponseModel();
        try{

            $page = $page <= 0 ? 1 : $page;
            $perpage = (int)$perpage ?? 0;
            $offset = ($page - 1) * $perpage;

            $listDm = $this->userDirectMessageRepository->getListLatestChatByUserUUid($userUUID, $offset, $perpage);
            /**
             * @var DirectMessageDto[]
            */

            $s3ExternalHost = env('S3_EXTERNAL_ENDPOINT');

            $listMsgDto = [];
            foreach($listDm as $msg){
                $dataDto = new DirectMessageDto();
                $dataDto->id = $msg->id;
                $dataDto->message = $msg->messageTxt;
                $dataDto->createdAt = $msg->createdAt;

                // sender
                $msg->sender->profilePictPath = $s3ExternalHost.'/'.$msg->sender->profilePictPath;
                $dataDto->sender = new AppUserDto(null, $msg->sender);

                // receiver
                $msg->receiver->profilePictPath = $s3ExternalHost.'/'.$msg->receiver->profilePictPath;
                $dataDto->receiver = new AppUserDto(null, $msg->receiver);

                array_push($listMsgDto,$dataDto);
            }

            $reponse->statusCode = "CHAT000";
            $reponse->message = "success";
            $reponse->data = $listMsgDto;

        }catch(Exception $ex){
            $reponse->statusCode = "CHAT500";
            $reponse->message = "Internal server error";
            $reponse->errorMessage = $reponse->message;
            dd($ex);
        }
        return $reponse;
    }
    
}


?>