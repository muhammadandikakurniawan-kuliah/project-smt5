<?php

namespace App\Usecases\Chat\Models;
use App\Dto\BaseResponseModel;
use App\Dto\DirectMessageDto;


class SendMessageResponseModel extends BaseResponseModel {

    /**
     * @var DirectMessageDto
    */
    public $data;

}

?>