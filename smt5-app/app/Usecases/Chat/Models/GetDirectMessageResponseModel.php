<?php

namespace App\Usecases\Chat\Models;
use App\Dto\BaseResponseModel;
use App\Dto\DirectMessageViewDto;


class GetDirectMessageResponseModel extends BaseResponseModel {

    /**
     * @var DirectMessageViewDto
    */
    public $data;

}

?>