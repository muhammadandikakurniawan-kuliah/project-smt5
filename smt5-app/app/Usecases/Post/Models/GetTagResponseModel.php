<?php
namespace App\Usecases\Post\Models;
use App\Dto\BaseResponseModel;
use App\Dto\UserPostDto;

class GetTagResponseModel extends BaseResponseModel {
    
    /**
     * @var string[]
    */
    public $data;

    /**
     * @var array
    */
    public $validationErrors;
}

?>