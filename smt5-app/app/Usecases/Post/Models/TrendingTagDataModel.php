<?php
namespace App\Usecases\Post\Models;

class TrendingTagDataModel {
    
    /**
     * @var string
    */
    public $tag;

    /**
     * @var string
    */
    public $link;

    /**
     * @var int
    */
    public $count;

}

?>