<?php
namespace App\Usecases\Post\Models;

class CreatePostRequestModel {
    
    /**
     * @var string
    */
    public $userUUID;

    /**
     * @var string
    */
    public $postUUID;

    /**
     * @var string
    */
    public $textContent;

    /**
     * @var PostFileModel[]
    */
    public $files = [];

}

?>