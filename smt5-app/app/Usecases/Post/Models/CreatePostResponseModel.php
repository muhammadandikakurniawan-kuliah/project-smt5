<?php
namespace App\Usecases\Post\Models;
use App\Dto\BaseResponseModel;
use App\Dto\UserPostDto;

class CreatePostResponseModel extends BaseResponseModel{

    /**
     * @var ?UserPostDto
    */
    public $data;

    /**
     * @var array
    */
    public $validationErrors;

}
?>