<?php 
namespace App\Usecases\Post\Models;

class PostFileModel {
    /**
     * @var string
    */
    public $fileBase64;

    /**
     * @var string
    */
    public $fileName;

    /**
     * @var string
    */
    public $fileType;

    /**
     * @var string
    */
    public $filePath;

    
}

?>