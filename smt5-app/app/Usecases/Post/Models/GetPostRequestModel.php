<?php
namespace App\Usecases\Post\Models;

class GetPostRequestModel {
    
    /**
     * @var string
    */
    public $userUUID;

    /**
     * @var string
    */
    public $postUUID;

    /**
     * @var int
    */
    public $perpage = 0;

    /**
     * @var string
    */
    public $search = "";

    /**
     * @var string
    */
    public $tag = "";

    /**
     * @var string
    */
    public $searchType = "";

    /**
     * @var int
    */
    public $page = 0;

}

?>