<?php
namespace App\Usecases\Post;

use App\Dto\AppUserDto;
use App\Dto\BaseResponseModel;
use App\Dto\PostCommentDto;
use App\Dto\UserPostDto;
use App\Dto\UserPostFileDto;
use App\Entities\ContentTagEntity;
use App\Entities\PostCommentEntity;
use App\Entities\PostTagEntity;
use App\Infrastructures\MongoRepositories\ContentTagRepository;
use App\Infrastructures\MongoRepositories\UserPostTagRepository;
use App\Infrastructures\Repositories\PostCommentRepository;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\UserPostFileRepository;
use App\Infrastructures\Repositories\UserPostRepository;
use App\Usecases\Post\Models\CreatePostRequestModel;
use App\Usecases\Post\Models\CreatePostResponseModel;
use App\Usecases\Post\Models\GetPostRequestModel;
use App\Usecases\Post\Models\GetPostResponseModel;
use App\Usecases\Post\Models\GetTagResponseModel;
use Exception;
use App\Helpers\S3Helper;
use App\Models\UserPostModel;
use App\Entities\UserPostEntity;
use App\Entities\UserPostFileEntity;
use App\Usecases\Post\Models\PostFileModel;
use App\Usecases\Post\Models\TrendingTagDataModel;

class PostUsecase {
    /**
     * @var PostUsecase
    */
    private static $instance = null;

    /**
     * @var UserPostRepository
    */
    private $userPostRepository;

    /**
     * @var UserPostFileRepository
    */
    private $userPostFileRepository;

    /**
     * @var AppUserRepository
    */
    private $appUserRepository;

    /**
     * @var S3Helper
    */
    private $s3Helper;

    /**
     * @var UserPostModel
    */
    private $userPostModel;

    /**
     * @var UserPostTagRepository
    */
    private $userPostTagRepository;

    /**
     * @var ContentTagRepository
    */
    private $contentTagRepository;

    /**
     * @var PostCommentRepository
    */
    private $postCommentRepository;


    public function __construct(
        UserPostRepository $userPostRepo,
        UserPostFileRepository $userPostFileRepo,
        S3Helper $s3Helper,
        UserPostModel $userPostModel,
        AppUserRepository $appUserRepository,
        UserPostTagRepository $userPostTagRepository,
        ContentTagRepository $contentTagRepository,
        PostCommentRepository $postCommentRepository
    )
    {
        $this->userPostRepository = $userPostRepo;
        $this->userPostFileRepository = $userPostFileRepo;
        $this->s3Helper = $s3Helper;
        $this->userPostModel = $userPostModel;
        $this->appUserRepository = $appUserRepository;
        $this->userPostTagRepository = $userPostTagRepository;
        $this->contentTagRepository = $contentTagRepository;
        $this->postCommentRepository = $postCommentRepository;
    }

    public static function getInstance() : PostUsecase{
        if(empty(self::$instance)) {
            self::$instance = new PostUsecase(
                UserPostRepository::getInstance(),
                UserPostFileRepository::getInstance(),
                S3Helper::getInstance(),
                UserPostModel::getInstance(),
                AppUserRepository::getInstance(),
                UserPostTagRepository::getInstance(),
                ContentTagRepository::getInstance(),
                PostCommentRepository::getInstance()
            );
        }
        return self::$instance;
    }

    /**
     * @param string
     * @return string[]
    */
    private function getContentTags(string $textContent) : array{
        $textContent = str_replace(['</p><p>'], ' ', $textContent);
        $textContent = strip_tags($textContent);
        preg_match_all('/#[^\s][^39;.+]\S+/', $textContent, $matches);
        if(empty($matches)){
            dd($matches);
            return [];
        }

        $tags = array_map(function($d){
            return str_replace(['#','&nbsp;'],'',$d);
        },$matches[0]);
    
        return $tags;
    }

    public function createPost(CreatePostRequestModel $request) : CreatePostResponseModel{
        $response = new CreatePostResponseModel();
        $response->validationErrors = [];
        try{

            // validation

            if(empty($request->textContent) && empty($request->files)){
                $response->statusCode = "POST400";
                $response->message = "Invalid request";
                array_push($response->validationErrors, "invalid request");
                return $response;
            }

            // end validation

            $this->userPostModel->db->transBegin();

            $userPostEntity = new UserPostEntity();
            $userPostEntity->textContent = $request->textContent;
            $userPostEntity->userUuid = $request->userUUID;

            $userPostEntity = $this->userPostRepository->create($userPostEntity);
            if(!($userPostEntity instanceof UserPostEntity)){
                $response->statusCode = "POST500";
                $response->message = "Create post failed";
                return $response;
            }

            $this->setContentTags($userPostEntity->uuid, $userPostEntity->textContent);

            $postFiles = $this->uploadFiles($userPostEntity->uuid, $request->userUUID, $request->files);
            if(count($postFiles) > 0){
                $this->userPostFileRepository->createBulk($postFiles);
            }

            $this->userPostModel->db->transComplete();
            $response->statusCode = "POST000";
            $response->message = "Sucess";

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }

    private function setContentTags(string $postUUID, string $textContent){
        $tags = $this->getContentTags($textContent);
        if(empty($tags)){
            return;
        }
        $postTagData = $this->userPostTagRepository->getByPostUUID($postUUID);
        if($postTagData instanceof PostTagEntity){
            $postTagData->tags = $tags;
            $this->userPostTagRepository->updateByPostUUID($postTagData);
        }else{
            $postTagData = new PostTagEntity();
            $postTagData->tags = $tags;
            $postTagData->postUuid = $postUUID;
            $this->userPostTagRepository->create($postTagData);
        }
        $this->insertContentTag($tags);
        return true;
    }

    /**
     * @param string[]
    */
    private function insertContentTag(array $tags){
        /**
         * @var ContentTagEntity[]
        */
        $dataInsert = [];

        foreach($tags as $tag){
            $dataEntity = new ContentTagEntity();
            $dataEntity->tag = $tag;
            array_push($dataInsert, $dataEntity);
        }
        $this->contentTagRepository->createMany($dataInsert);
    }

    /**
     * @param PostFileModel[] $files
     * @return UserPostFileEntity[]
    */
    private function uploadFiles(string $postUUID, string $userUUUID, array $files) : array{
        /**
         * @var UserPostFileEntity[]
        */
        $result = [];

        $now = date('Y_m_d_H_i_s');
        $path = getenv("APP_USER_BUCKET").'/'.$userUUUID.'/'.$now;
        for($i = 0; $i < count($files); $i++){
            $file = $files[$i];
            $fileExt = '';
            
            $fileNameChunk = explode('.', $file->fileName);
            $fileNameChunkLength = count($fileNameChunk);
            if($fileNameChunkLength > 1){
                $fileExt = $fileNameChunk[($fileNameChunkLength-1)];
            }

            $filePath = $path.'-'.$i.'.'.$fileExt;

            $image_base64 = base64_decode($file->fileBase64);
            $this->s3Helper->upload($filePath, $image_base64, $file->fileType);

            $res = new UserPostFileEntity();
            $res->fileName = $file->fileName;
            $res->filePath = $filePath;
            $res->fileType = $file->fileType;
            $res->postUuid = $postUUID;
            array_push($result, $res);
        }
        return $result;
    }

    public function searchTag(GetPostRequestModel $request) : GetTagResponseModel{
        $response = new GetTagResponseModel();
        try{
          
            $page = $request->page <= 0 ? 1 : $request->page;
            $request->perpage = (int)$request->perpage ?? 0;
            $offset = ($page - 1) * $request->perpage;

            $listTag = $this->contentTagRepository->search($request->tag, $request->perpage, $offset);

            $response->statusCode = "POST000";
            $response->message = "success";
            $response->data = array_map(function($d){
                return  $d->tag;
            }, $listTag);

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }
        return $response;
    }

    public function getPostByTag(GetPostRequestModel $request) : GetPostResponseModel{
        $response = new GetPostResponseModel();
        try{
          
            $page = $request->page <= 0 ? 1 : $request->page;
            $request->perpage = (int)$request->perpage ?? 0;
            $offset = ($page - 1) * $request->perpage;

            $listPostTag = $this->userPostTagRepository->getByTag($request->tag, $request->perpage, $offset);
            $listPostUUID = array_map(function($d){return $d->postUuid;}, $listPostTag);

            $listPost = $this->userPostRepository->getByListPostUUID($listPostUUID);
            $listPostUUid = array_map(function($data){return $data->uuid;}, $listPost);
            $userUuid = array_map(function($data){return $data->userUuid;}, $listPost);

            $listPostFile = $this->userPostFileRepository->getByListPostUUID($listPostUUid);

            $listUser = $this->appUserRepository->getByListUUID($userUuid);

            $s3ExternalHost = getenv("S3_EXTERNAL_ENDPOINT");
            $listPostDto = [];
            for($iPost = 0; $iPost < count($listPost); $iPost++){
                $post = $listPost[$iPost];

                // set files
                $postDto = new UserPostDto(null, $post);
                $postDto->user = new AppUserDto();
                for($iFile = 0; $iFile < count($listPostFile); $iFile++){
                    $postFile = $listPostFile[$iFile];
                    if($postFile->postUuid == $post->uuid){
                        $postFileDto = new UserPostFileDto(null, $postFile);
                        $postFileDto->filePath = $s3ExternalHost.'/'.$postFileDto->filePath;
                        
                        $fileType = explode('/',$postFileDto->fileType)[0] ?? '';
                        $fileType = strtolower($fileType);
                        $postFileDto->isImage = $fileType == "image";
                        $postFileDto->isVideo = $fileType == "video";

                        array_push($postDto->files, $postFileDto);
                    }
                }  

                // set data user
                for($userIdx = 0; $userIdx < count($listUser); $userIdx++){
                    $userData = $listUser[$userIdx];
                    if($postDto->userUuid == $userData->uuid){
                        $userDto = new AppUserDto(null, $userData);
                        $userDto->profilePictPath = $s3ExternalHost.'/'.$userDto->profilePictPath;
                        $postDto->user = $userDto;
                    }
                }

                array_push($listPostDto, $postDto);
            }   

            $response->statusCode = "POST000";
            $response->message = "success";
            $response->data = $listPostDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }
        return $response;
    }

    public function getPost(GetPostRequestModel $request) : GetPostResponseModel{
        $response = new GetPostResponseModel();
        try{
            if(!empty($request->tag)){
                return $this->getPostByTag($request);
            }

            $page = $request->page <= 0 ? 1 : $request->page;
            $request->perpage = (int)$request->perpage ?? 0;
            $offset = ($page - 1) * $request->perpage;
            $search = $request->search;

            // var_dump($search);
            // var_dump($offset);
            // var_dump($request->perpage);
            // var_dump($request);
            // dd($request);

            $listPost = $this->userPostRepository->search($search, $request->perpage, $offset);
            $listPostUUid = array_map(function($data){return $data->uuid;}, $listPost);
            $userUuid = array_map(function($data){return $data->userUuid;}, $listPost);

            $listPostFile = $this->userPostFileRepository->getByListPostUUID($listPostUUid);

            $listUser = $this->appUserRepository->getByListUUID($userUuid);

            $s3ExternalHost = getenv("S3_EXTERNAL_ENDPOINT");
            $listPostDto = [];
            for($iPost = 0; $iPost < count($listPost); $iPost++){
                $post = $listPost[$iPost];

                // set files
                $postDto = new UserPostDto(null, $post);
                $postDto->user = new AppUserDto();
                for($iFile = 0; $iFile < count($listPostFile); $iFile++){
                    $postFile = $listPostFile[$iFile];
                    if($postFile->postUuid == $post->uuid){
                        $postFileDto = new UserPostFileDto(null, $postFile);
                        $postFileDto->filePath = $s3ExternalHost.'/'.$postFileDto->filePath;
                        
                        $fileType = explode('/',$postFileDto->fileType)[0] ?? '';
                        $fileType = strtolower($fileType);
                        $postFileDto->isImage = $fileType == "image";
                        $postFileDto->isVideo = $fileType == "video";

                        array_push($postDto->files, $postFileDto);
                    }
                }  

                // set data user
                for($userIdx = 0; $userIdx < count($listUser); $userIdx++){
                    $userData = $listUser[$userIdx];
                    if($postDto->userUuid == $userData->uuid){
                        $userDto = new AppUserDto(null, $userData);
                        $userDto->profilePictPath = $s3ExternalHost.'/'.$userDto->profilePictPath;
                        $postDto->user = $userDto;
                    }
                }

                array_push($listPostDto, $postDto);
            }   

            $response->statusCode = "POST000";
            $response->message = "success";
            $response->data = $listPostDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }
        return $response;
    }

    public function getListPostByUser(string $userUUID) : GetPostResponseModel{
        $response = new GetPostResponseModel();
        try{

            $listPost = $this->userPostRepository->getByUserUUID($userUUID);
            $listPostUUid = array_map(function($data){return $data->uuid;}, $listPost);
            $userUuid = array_map(function($data){return $data->userUuid;}, $listPost);

            $listPostFile = $this->userPostFileRepository->getByListPostUUID($listPostUUid);

            $listUser = $this->appUserRepository->getByListUUID($userUuid);

            $s3ExternalHost = getenv("S3_EXTERNAL_ENDPOINT");
            $listPostDto = [];
            for($iPost = 0; $iPost < count($listPost); $iPost++){
                $post = $listPost[$iPost];

                // set files
                $postDto = new UserPostDto(null, $post);
                $postDto->user = new AppUserDto();
                for($iFile = 0; $iFile < count($listPostFile); $iFile++){
                    $postFile = $listPostFile[$iFile];
                    if($postFile->postUuid == $post->uuid){
                        $postFileDto = new UserPostFileDto(null, $postFile);
                        $postFileDto->filePath = $s3ExternalHost.'/'.$postFileDto->filePath;
                        
                        $fileType = explode('/',$postFileDto->fileType)[0] ?? '';
                        $fileType = strtolower($fileType);
                        $postFileDto->isImage = $fileType == "image";
                        $postFileDto->isVideo = $fileType == "video";

                        array_push($postDto->files, $postFileDto);
                    }
                }  

                // set data user
                for($userIdx = 0; $userIdx < count($listUser); $userIdx++){
                    $userData = $listUser[$userIdx];
                    if($postDto->userUuid == $userData->uuid){
                        $userDto = new AppUserDto(null, $userData);
                        $userDto->profilePictPath = $s3ExternalHost.'/'.$userDto->profilePictPath;
                        $postDto->user = $userDto;
                    }
                }

                array_push($listPostDto, $postDto);
            }   

            $response->statusCode = "POST000";
            $response->message = "success";
            $response->data = $listPostDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }
        return $response;
    }

    public function getPostDetailByUUID(string $postUUID) : ?UserPostDto {
        $postData = $this->userPostRepository->getByUUID($postUUID);
        if(!$postData){
            return null;
        }

        $postDto = new UserPostDto(null, $postData);

        // get files
        $listPostFile = $this->userPostFileRepository->getByListPostUUID([$postDto->uuid]);
        $postDto->files = array_map(function($data){
            $fileDto = new UserPostFileDto(null,$data);
            $fileDto->filePath = getenv("S3_EXTERNAL_ENDPOINT").'/'.$fileDto->filePath;
            $fileType = explode('/',$fileDto->fileType)[0] ?? '';
            $fileType = strtolower($fileType);
            $fileDto->isImage = $fileType == "image";
            $fileDto->isVideo = $fileType == "video";
            return $fileDto;
        },$listPostFile);

        // get user
        $listUser = $this->appUserRepository->getByListUUID([$postDto->userUuid]);
        if(empty($listUser)){
            return null;
        }

        $userDto = new AppUserDto(null, $listUser[0]);
        $userDto->profilePictPath = getenv("S3_EXTERNAL_ENDPOINT").'/'.$userDto->profilePictPath;
        $postDto->user = $userDto;
        
        return $postDto;
    }


    public function getPostDetail(string $postUUID, int $commentPage = 1, int $commentLength = 3) : GetPostResponseModel{
        $response = new GetPostResponseModel();
        $response->validationErrors = [];
        try{

            $postData = $this->getPostDetailByUUID($postUUID);
            if(!$postData){
                $response->statusCode = "POST404";
                $response->message = "Not found";
                array_push($response->validationErrors, "Data not found");
                return $response;
            }

            // commentPagination
            $commentPage = $commentPage <= 0 ? 1: $commentPage;
            $commentOffset = ($commentPage-1)*$commentLength;
            $listComment = $this->postCommentRepository->getByPostUUID($postUUID, $commentLength, $commentOffset);
            
            /**
             * @var PostCommentDto[]
            */
            $listCommentDto = [];
            foreach($listComment as $commentData){
                $commentDto = new PostCommentDto($commentData);
                $commentData->user->profilePictPath = getenv("S3_EXTERNAL_ENDPOINT").'/'.$commentData->user->profilePictPath;
                $commentDto->user = new AppUserDto(null, $commentData->user);
                array_push($listCommentDto, $commentDto);
            }

            $postData->comments =  $listCommentDto;
            $response->statusCode = "POST000";
            $response->message = "success";
            $response->data = [$postData];

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }
        return $response;
    }

    public function updatePost(CreatePostRequestModel $request) : CreatePostResponseModel{
        $response = new CreatePostResponseModel();
        $response->validationErrors = [];
        try{

            // validation
            $postData = $this->userPostRepository->getByUUID($request->postUUID);
            
            if($postData == null || $postData->userUuid != $request->userUUID){
                $response->statusCode = "POST404";
                $response->message = "data not found";
                $response->errorMessage = "data not found";
                return $response;
            }

            // end validation
            $postData->textContent = $request->textContent;
            $postData = $this->userPostRepository->update($postData);
            if(!($postData instanceof UserPostEntity)){
                $response->statusCode = "POST500";
                $response->message = "Update post failed";
                dd($response);
                return $response;
            }
            
            $postDto = $this->getPostDetailByUUID($request->postUUID);

            $this->setContentTags($postDto->uuid, $postDto->textContent);
            $response->statusCode = "POST000";
            $response->message = "Sucess";
            $response->data = $postDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }

    public function delete(string $postUUID, string $userUUID) : CreatePostResponseModel{
        $response = new CreatePostResponseModel();
        $response->validationErrors = [];
        try{

            // validation
            $postData = $this->userPostRepository->getByUUID($postUUID);
            
            if($postData == null || $postData->userUuid != $userUUID){
                $response->statusCode = "POST404";
                $response->message = "data not found";
                $response->errorMessage = "data not found";
                return $response;
            }

            // end validation
            $postData->deletedAt = date('Y-m-d H:i:s');
            $postData = $this->userPostRepository->update($postData);
            if(!($postData instanceof UserPostEntity)){
                $response->statusCode = "POST500";
                $response->message = "Update post failed";
                dd($response);
                return $response;
            }
            $postDto = $this->getPostDetailByUUID($postUUID);
            $response->statusCode = "POST000";
            $response->message = "Sucess";
            $response->data = $postDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }

    public function AddComment( string $postUUID, string  $userUUID, string  $commentTxt) : BaseResponseModel{
        $response = new BaseResponseModel();
        try{

            $postData = $this->getPostDetailByUUID($postUUID);
            if(!$postData){
                $response->statusCode = "POST404";
                $response->message = "Not found";
                return $response;
            }

            // get user
            $listUser = $this->appUserRepository->getByListUUID([$userUUID]);
            if(empty($listUser)){
                $response->statusCode = "POST404";
                $response->message = "Not found";
                return $response;
            }
            $userDto = $listUser[0];
            $userDto->profilePictPath = getenv("S3_EXTERNAL_ENDPOINT").'/'.$userDto->profilePictPath;

            // end validation
            $commentEntity = new PostCommentEntity();
            $commentEntity->userUUID = $userUUID;
            $commentEntity->postUUID = $postUUID;
            $commentEntity->commentTxt = $commentTxt;

            $commentEntity = $this->postCommentRepository->create($commentEntity);
            $commentDto = new PostCommentDto($commentEntity);
            $commentDto->user = new AppUserDto(null, $userDto);

            $response->statusCode = "POST000";
            $response->message = "Sucess";
            $response->data = $commentDto;

        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = "internal server error";
        }
        return $response;
    }


    public function getTrandingTag() : BaseResponseModel{
        $response = new BaseResponseModel();
        $response->data = [];
        try{
            $resultData = [];
            $baseUrl = base_url();
            $trandingTags = $this->contentTagRepository->getTrandingTags(10);
            foreach($trandingTags as $tag){
                $data = new TrendingTagDataModel();
                $data->tag = '#'.$tag->tag;
                $data->link = $baseUrl.'?tag='.$tag->tag;
                $data->count = $tag->count;
                array_push($response->data, $data);
            }
            $response->statusCode = "POST000";
            $response->message = "Sucess";
        }catch(Exception $ex){
            $response->statusCode = "POST500";
            $response->message = "Internal server error";
            $response->errorMessage = "internal server error";
        }

        return $response;
    }
    
}

?>