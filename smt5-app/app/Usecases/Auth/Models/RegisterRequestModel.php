<?php
namespace App\Usecases\Auth\Models;

class RegisterRequestModel {

    /**
     * @var string
    */
    public $email;

    /**
     * @var string
    */
    public $username;

    /**
     * @var string
    */
    public $firstName;

    /**
     * @var string
    */
    public $lastName;

    /**
     * @var string
    */
    public $phoneNumber;

    /**
     * @var string
    */
    public $password;

    /**
     * @var string
    */
    public $confirmationPasword;

    /**
     * @var string
    */
    public $profilePict;

    /**
     * @var string
    */
    public $coverPict;

    public function toArray() : array {
        return [
            "firstName" => $this->firstName,
            "lastName" => $this->lastName,
            "username" => $this->username,
            "password" => $this->password,
            "confirmationPasword" => $this->confirmationPasword,
            "email" => $this->email,
            "phone_number" => $this->phoneNumber,
            "profile_pict_path" => $this->profilePict,
            "cover_pict_path" => $this->coverPict
        ];
    }
}

?>