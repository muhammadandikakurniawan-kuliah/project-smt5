<?php
namespace App\Usecases\Auth\Models;

class GetUserRequestModel {
    
    /**
     * @var string
    */
    public $userUUID;

    /**
     * @var int
    */
    public $perpage = 0;

    /**
     * @var string
    */
    public $search = "";

    /**
     * @var string
    */
    public $searchType = "";

    /**
     * @var int
    */
    public $page = 0;

}

?>