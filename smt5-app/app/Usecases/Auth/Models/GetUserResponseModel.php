<?php
namespace App\Usecases\Auth\Models;
use App\Dto\AppUserDto;
use App\Dto\BaseResponseModel;

class GetUserResponseModel extends BaseResponseModel{

    /**
     * @var AppUserDto[]
    */
    public $data = [];

}

?>