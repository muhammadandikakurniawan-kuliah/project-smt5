<?php 
namespace App\Usecases\Auth\Models;

use App\Dto\BaseResponseModel;
use App\Dto\AppUserDto;

class RegisterResponseModel extends BaseResponseModel{

    /**
     * @var ?AppUserDto
    */
    public $data;

    /**
     * @var array
    */
    public $validationErrors;

}

?>