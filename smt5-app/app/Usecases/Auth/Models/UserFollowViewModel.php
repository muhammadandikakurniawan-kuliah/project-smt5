<?php
namespace App\Usecases\Auth\Models;
use App\Dto\AppUserDto;

class UserFollowViewModel {
    
    /**
     * @var int
    */
    public $id;

    /**
     * @var AppUserDto
    */
    public $user;

    /**
     * @var AppUserDto
    */
    public $followedUser;

}

?>