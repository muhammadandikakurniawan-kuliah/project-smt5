<?php
namespace App\Usecases\Auth;
use App\Entities\UserFollowEntity;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\UserFollowRepository;
use App\Models\UserFollowModel;
use App\Usecases\Auth\Models\GetUserRequestModel;
use App\Usecases\Auth\Models\GetUserResponseModel;
use App\Usecases\Auth\Models\UserFollowViewModel;
use CodeIgniter\Encryption\Encryption;
use App\Entities\AppUserEntity;
use Config\Services;
use App\Usecases\Auth\Models\RegisterRequestModel;
use App\Usecases\Auth\Models\RegisterResponseModel;
use Exception;
use App\Helpers\EmailHelper;
use App\Helpers\S3Helper;
use App\Models\AppUserModel;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Spatie\Async\Pool;
use App\Dto\AppUserDto;
use App\Dto\BaseResponseModel;

class AuthUsecase {

    /**
     * @var AuthUsecase
     */
    private static $instance = null;

    /**
     * @var AppUserRepository
     */
    private $appUserRepository;

    /**
     * @var EmailHelper
     */
    private $emailHelper;

    /**
     * @var S3Helper
    */
    private $s3Helper;

    /**
     * @var AppUserModel
    */
    private $appUserModel;

    /**
     * @var UserFollowRepsitory
     */
    private $userFollowRepository;

    public static function getInstance() : AuthUsecase{
        if(empty(self::$instance)) {
            self::$instance = new AuthUsecase(
                AppUserRepository::getInstance(),
                EmailHelper::getInstance(),
                S3Helper::getInstance(),
                AppUserModel::getInstance(),
                UserFollowRepository::getInstance()
            );
        }
        return self::$instance;
    }

    /**
     * @param AppUserRepository $appUserRepo
    */
    function __construct(
        AppUserRepository $appUserRepo,
        EmailHelper $emailHelper,
        S3Helper $s3Helper,
        AppUserModel $appUserModel,
        UserFollowRepository $userFollowRepository
    )
    {
        $this->appUserRepository = $appUserRepo;
        $this->emailHelper = $emailHelper;
        $this->s3Helper = $s3Helper;
        $this->appUserModel = $appUserModel;
        $this->userFollowRepository = $userFollowRepository;
    }

    public function activateAccount(string $token) : RegisterResponseModel{
        $response = new RegisterResponseModel();
        $response->validationErrors = [];
        try{
            // validate
            $jwtKey = getenv("JWT_KEY");
            $payload = (array)JWT::decode($token, new Key($jwtKey, 'HS256'));

            $uuid = $payload["identifier"];
            $email = $payload["email"];
            $username = $payload["username"];

            if(!$uuid || !$email || !$username){
               array_push($response->validationErrors, "invalid data"); 
            }

            $userByEmail = $this->appUserRepository->findByEmail($email);
            if($userByEmail == null){
                array_push($response->validationErrors, "invalid data"); 
            }else{
                if($userByEmail->uuid != $uuid){
                    array_push($response->validationErrors, "invalid data"); 
                }else{
                    if($userByEmail->activatedAt != null){
                        array_push($response->validationErrors, "account already activated");
                    }
                }
            }
            if(count($response->validationErrors) > 0){
                $response->statusCode = "ACT400";
                $response->message = "invalid request";
                return $response; 
            }

            // update data
            $userByEmail->activatedAt = date('Y-m-d H:i:s');
            $updatedUser = $this->appUserRepository->update($userByEmail);
            if($updatedUser == null){
                $response->statusCode = "ACT501";
                $response->message = "internal server error";
                $response->errorMessage = "activate account failed";
                return $response;
            }

            $response->statusCode = "ACT000";
            $response->message = "success";
            $response->data = new AppUserDto(null, $updatedUser);

        }catch(Exception $ex){
            $response->statusCode = "ACT500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
        }

        return $response;
    }

    public function register(RegisterRequestModel $request) : RegisterResponseModel{
        $response = new RegisterResponseModel();
        $response->validationErrors = [];
        try{
            // validate
            
            if($this->isEmptyString($request->password)){
                array_push($response->validationErrors, "password cannot be empty");
            }
            if($request->confirmationPasword != $request->password){
                array_push($response->validationErrors, "confirmation password is not match with password");
            }
            if($this->isEmptyString($request->username)){
                array_push($response->validationErrors, "username cannot be empty");
            }
            if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
                array_push($response->validationErrors, "invalid email");
            }

            $userByEmail = $this->appUserRepository->findByEmail($request->email);
            if($userByEmail != null){
                array_push($response->validationErrors, "email already used");
            }
            $userByUsername = $this->appUserRepository->findByUsername($request->email);
            if($userByUsername != null){
                array_push($response->validationErrors, "username already used");
            }

            if(count($response->validationErrors) > 0){
                $response->statusCode = "REG400";
                $response->message = "invalid request";
                return $response; 
            }

            $this->appUserModel->db->transStart();

            // insert data user to db
            $defaultCoverPict = getenv('DEFAULT_COVER_PROFILE_PICT_PATH') ;
            $defaultProfilePict = getenv('DEFAULT_PROFILE_PICT_PATH');
            $userData = new AppUserEntity();
            $userData->longName = $request->firstName.' '.$request->lastName;
            $userData->username = $request->username;
            $userData->phoneNumber = $request->phoneNumber ?? '';
            $userData->coverPictPath = $defaultCoverPict;
            $userData->profilePictPath = $defaultProfilePict;
            $userData->phoneNumber = $request->phoneNumber ?? '';
            $userData->email = $request->email;
            $userData->password = $this->encryptPass($request->password);
            $newUserData = $this->appUserRepository->create($userData);
            if(!($newUserData instanceof AppUserEntity)){
                $response->statusCode = "REG500";
                $response->message = "failed";
                $response->errorMessage = "create user failed";
                dd($response);
                return $response;
            }

            // start set image
            if($request->profilePict != getenv('S3_EXTERNAL_ENDPOINT').'/'.$newUserData->profilePictPath){
                $path = $this->saveImage($newUserData->uuid, $request->profilePict,"profile");
                if(!$path){
                    $response->statusCode = "REG501";
                    $response->message = "failed";
                    $response->errorMessage = "set profile picture failed";
                    dd($path);
                    return $response;
                }
                $newUserData->profilePictPath = $path;
            }

            if($request->coverPict != getenv('S3_EXTERNAL_ENDPOINT').'/'.$newUserData->coverPictPath){
                $path = $this->saveImage($newUserData->uuid, $request->coverPict, "cover");
                if(!$path){
                    $response->statusCode = "REG501";
                    $response->message = "failed";
                    $response->errorMessage = "set cover picture failed";
                    dd($path);
                    return $response;
                }
                $newUserData->coverPictPath = $path;
            }
            // end set image

            $newUserData = $this->appUserRepository->update($newUserData);
            if(!($newUserData instanceof AppUserEntity)){
                $response->statusCode = "REG502";
                $response->message = "failed";
                $response->errorMessage = "update user failed";
                dd($response);
                return $response;
            }

            $this->appUserModel->db->transComplete();

            $response->statusCode = "REG000";
            $response->message = "success";
            $response->data = new AppUserDto(null,$newUserData);

            $this->sendActivationEmail($newUserData);

        }catch(Exception $ex){
            $response->statusCode = "REG500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }

    function sendActivationEmail(AppUserEntity $userData){
        try{
            $pool = Pool::create();
            $pool->add(function () use ($userData) {
                $key = getenv("JWT_KEY");
                $payload = [
                    'iss' => base_url(),
                    'aud' => base_url(),
                    'iat' => time(),
                    'email' => $userData->email,
                    'username' => $userData->username,
                    'identifier' => $userData->uuid
                ];
                $jwt = JWT::encode($payload,$key,'HS256');
                $this->emailHelper->sendActivationAccount($userData->email, $userData->username, $jwt);
            })->then(function ($res) {
                echo "Send activation email: ".$res;
            });;
        }catch(Exception $ex){
            dd($ex);
        }
    }
    private function saveImage(string $userUUid,string $imgBase64, string $pictType){

        $imageParts = explode(";base64,", $imgBase64);
        $image_base64 = base64_decode($imageParts[1]);
        $contentType = explode(":",$imageParts[0])[1];

        $contentTypeChunks = explode('/',$contentType);
        $dataType = "jpg";
        if(count($contentTypeChunks) > 0 ){
            $dataType = $contentTypeChunks[1];
        }

        $fileName = $pictType.date('Y_m_d_H_i_s').'.'.$dataType;
        $path = "app-user/$userUUid/$fileName";
        $this->s3Helper->upload($path, $image_base64, $contentType);
        return $path;
    }

    private function isEmptyString(string $str) : bool {
        return  preg_replace('/\s+/', '', $str) == "";
    }

    private function encryptPass(string $pass) : string {
        $config = new \Config\Encryption();
        $encryptor = Services::encrypter();
        $encryptedPass = $encryptor->encrypt($pass, array('cipher' => 'aes-256', 'mode' => 'cbc', 'hmac' => FALSE, 'key' => $config->key ));

        $encryptedPass = base64_encode($encryptedPass);
        return $encryptedPass;
    }

    private function decryptPass(string $pass) : string {
        
        $config = new \Config\Encryption();
        $encryptor = Services::encrypter();

        $pass = base64_decode($pass);
        $decryptedPass = $encryptor->decrypt($pass, array('cipher' => 'aes-256', 'mode' => 'cbc', 'hmac' => FALSE, 'key' => $config->key ));

        return $decryptedPass;
    }

    public function login(string $email, string $password) : RegisterResponseModel{
        $response = new RegisterResponseModel();
        $response->validationErrors = [];
        try{
            $userByEmail = $this->appUserRepository->findByEmail($email);
            if($userByEmail == null){
                array_push($response->validationErrors, "user not found"); 
            }else{
                if($userByEmail->activatedAt == null){
                    array_push($response->validationErrors, "account is inactive");
                }else{
                    $decryptedPass = $this->decryptPass($userByEmail->password);
                    if($decryptedPass != $password){
                        array_push($response->validationErrors, "password is wrong");
                    }
                }
            }

            if(count($response->validationErrors) > 0){
                $response->statusCode = "LOGIN400";
                $response->message = "invalid request";
                return $response; 
            }

            $response->statusCode = "LOGIN000";
            $response->message = "success";
            $response->data = new AppUserDto(null, $userByEmail);

        }catch(Exception $ex){
            $response->statusCode = "LOGIN500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }
    
    public function searchUser(GetUserRequestModel $request) : GetUserResponseModel{
        $response = new GetUserResponseModel();
        try{

            $page = $request->page <= 0 ? 1 : $request->page;
            $request->perpage = (int)$request->perpage ?? 0;
            $offset = ($page - 1) * $request->perpage;
            $search = $request->search;

            $listUser = $this->appUserRepository->search($search, $request->perpage, $offset);

            $response->statusCode = "AUTH000";
            $response->message = "success";
            $response->data = array_map(function($data){
                $dataDto = new AppUserDto(null, $data);
                $dataDto->profilePictPath = getenv('S3_EXTERNAL_ENDPOINT').'/'.$dataDto->profilePictPath;
                return $dataDto;
            }, $listUser);

        }catch(Exception $ex){
            $response->statusCode = "AUTH500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
        }

        return $response;
    }

    public function getUserByUUID(string $userUUID, string $userLoginUUID = "") : GetUserResponseModel{
        $response = new GetUserResponseModel();
        try{
            $userData = $this->appUserRepository->getByUUID($userUUID);
            if(!($userData instanceof AppUserEntity)){
                $response->statusCode = "AUTH204";
                $response->message = "not found";
                $response->errorMessage = "not found";
                return $response;
            }

            $dataDto = new AppUserDto(null, $userData);
            $dataDto->profilePictPath = getenv('S3_EXTERNAL_ENDPOINT').'/'.$dataDto->profilePictPath;
            $dataDto->coverPictPath = getenv('S3_EXTERNAL_ENDPOINT').'/'.$dataDto->coverPictPath;

            if($userLoginUUID){
                $userFollowData = $this->userFollowRepository->getByUserAndFollowewdId($userLoginUUID, $userUUID);
                $dataDto->isFollowed = $userFollowData instanceof  UserFollowEntity;
            }

            $dataDto->followersCount = $this->userFollowRepository->getFollowersCount($userUUID);
            $dataDto->followingCount = $this->userFollowRepository->getFollowingCount($userUUID);

            $response->statusCode = "AUTH000";
            $response->message = "success";
            $response->data = [$dataDto];

        }catch(Exception $ex){
            $response->statusCode = "AUTH500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
            dd($ex);
        }

        return $response;
    }

    public function followUser(string $userLoginId, string $followeedUserId, bool $follow) : BaseResponseModel{
        $response = new BaseResponseModel();
        try{
            $userFollowData = new UserFollowEntity();
            $userFollowData->userUUID = $userLoginId;
            $userFollowData->userFollowUUID = $followeedUserId;

            $currentData = $this->userFollowRepository->getByUserAndFollowewdId($userLoginId, $followeedUserId, false);
            if($follow){
                if($currentData instanceof UserFollowEntity){
                    $currentData->deletedAt = null;
                    $userFollowData = $this->userFollowRepository->update($currentData);
                }else{
                    $userFollowData = $this->userFollowRepository->create($userFollowData);
                }
            }else{
                if(($currentData instanceof UserFollowEntity)){
                    $currentData->deletedAt = date('Y-m-d H:i:s');
                    $userFollowData = $this->userFollowRepository->update($currentData);
                }else{
                    $userFollowData = $this->userFollowRepository->create($userFollowData);
                }
            }

            $response->statusCode = "USR000";
            $response->message = "success";
            $response->data = new UserFollowViewModel();
            $response->data->id = $userFollowData->id;
        }catch(Exception $ex){
            $response->statusCode = "USR500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
        }
        return $response;
    }

    public function getfollowUserData(string $userLoginId, bool $isFollowers = false) : GetUserResponseModel{
        $response = new GetUserResponseModel();
        try{
            $listFollowData = [];
            if($isFollowers){
                $listFollowData = $this->userFollowRepository->getFollowers($userLoginId);
            }else{
                $listFollowData = $this->userFollowRepository->getFollowing($userLoginId);
            }

            $response->data = array_map(function($data){
                $dataDto = new AppUserDto(null, $data->user);
                $dataDto->profilePictPath = getenv('S3_EXTERNAL_ENDPOINT').'/'.$dataDto->profilePictPath;
                return $dataDto;
            }, $listFollowData);

            $response->statusCode = "USR000";
            $response->message = "success";
        }catch(Exception $ex){
            $response->statusCode = "USR500";
            $response->message = "Internal server error";
            $response->errorMessage = $ex;
        }
        return $response;
    }
}

?>