<?php

namespace App\Helpers;
use CodeIgniter\Email\Email;
use Exception;

class EmailHelper {

    /**
     * @var \Config\Email 
    */
    private $emailConfig;

    /**
     * @var S3Helper 
    */
    private $s3Helper;

    /**
     * EmailHelper
    */
    private static $instance = null;

    public static function  getInstance() : EmailHelper{

        if(self::$instance == null){
            self::$instance = new EmailHelper();
        }

        return self::$instance;
    }

    function __construct()
    {
        $this->emailConfig = new \Config\Email();
        $this->emailConfig->protocol = getenv("EMAIL_PROTOCOL");
        $this->emailConfig->SMTPHost = getenv("EMAIL_SMTP_HOST");
        $this->emailConfig->SMTPPort = getenv("EMAIL_SMTP_PORT");
        $this->emailConfig->SMTPUser = getenv("EMAIL_SMTP_USER");
        $this->emailConfig->SMTPPass = getenv("EMAIL_SMTP_PASSWORD");
        $this->emailConfig->SMTPCrypto = getenv("EMAIL_SMTP_CRYPTO");
        $this->emailConfig->mailType = 'html';
        $this->emailConfig->charset = 'utf-8';

        // $emailConfig->protocol = 'smtp';
        // $emailConfig->SMTPHost = 'smtp.gmail.com';
        // $emailConfig->SMTPPort = 465;
        // $emailConfig->SMTPUser = 'project.local000@gmail.com';
        // $emailConfig->SMTPPass = 'jiuwdpadjtebtzko';
        // $emailConfig->SMTPCrypto = 'ssl';
        // $emailConfig->mailType = 'html';
        // $emailConfig->charset = 'utf-8';

        $this->s3Helper = S3Helper::getInstance();
    }

    function sendActivationAccount(string $userEmail, string $username, string $activationToken) : bool {

        $isSuccess = false;

        try{
            
            $EmailTemplate = $this->s3Helper->getFile(getenv("EMAIL_TEMPLATE_PATH"));

            $activationLink = getenv("ACTIVATION_LINK")."/".$activationToken;

            $EmailTemplate = str_replace("{{link-activation}}", $activationLink, $EmailTemplate);
            $EmailTemplate = str_replace("{{username}}", $username, $EmailTemplate);

            $email = new Email($this->emailConfig);

            $email->setNewline("\r\n");
            $email->setFrom('project.local000@gmail.com');
            $email->setTo($userEmail);
            $email->setSubject('Kelompok5App Activation Account');
            $email->setMessage($EmailTemplate);
            

            $isSuccess = $email->send();
            if(!$isSuccess){
                dd($email->printDebugger());
            }

        }catch(Exception $ex){
            $isSuccess = false;
            dd($ex);
        }

        return $isSuccess;

    }

}

?>