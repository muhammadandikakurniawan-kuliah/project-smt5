<?php

namespace App\Helpers;
use CodeIgniter\Email\Email;
use Exception;
use Aws\S3\S3Client;
use Aws\Crypto\Polyfill\ByteArray;

class S3Helper {

    /**
     * @var S3Client 
    */
    private $s3Client;

    /**
     * @var Email
    */
    private $email;

    /**
     * S3Helper
    */
    private static $instance = null;

    public static function  getInstance() : S3Helper{

        if(self::$instance == null){
            self::$instance = new S3Helper();
        }

        return self::$instance;
    }

    function __construct()
    {
        $this->s3Client = new S3Client([
            "version" => getenv("S3_VERSION"),
            "endpoint" => getenv("S3_ENDPOINT"),
            'region'  => getenv("S3_REGION"),
            "credentials" => [
                "key"    => getenv("S3_USER"),
                "secret" => getenv("S3_PASSWORD")
            ]
        ]);
    }

    function getFile(string $path) {
        $pathChunk = explode('/',$path);
        if(count($pathChunk) < 2){
            return false;
        }

        $bucket = $pathChunk[0];
        $key = join('/', array_slice($pathChunk,1));

        $object = $this->s3Client->getObject([
            'Bucket' => $bucket,
            'Key'    => $key,
        ]);

        $body = $object["Body"]->read($object['ContentLength']);

        return $body;
    }

    public function upload(string $path, $data, string $contentType){

        $pathChunk = explode('/',$path);
        if(count($pathChunk) < 2){
            return false;
        }

        $bucket = $pathChunk[0];
        $key = join('/', array_slice($pathChunk,1));

        return $this->s3Client->putObject([
            'Bucket'          => $bucket,
            'Key'             => $key,
            'Body'            => $data,
            'ContentType'     => $contentType,
            'ACL'             => 'public-read'
        ]);
    }

}

?>