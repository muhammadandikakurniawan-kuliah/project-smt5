<?php
namespace App\Models;

use CodeIgniter\Model;

class UserDirectMessageModel extends Model{
    protected $table = "user_direct_messages";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var UserDirectMessageModel
    */
    private static $instance = null;

    public static function getInstance() : UserDirectMessageModel
    {
        if (self::$instance == null) {
            self::$instance = new UserDirectMessageModel();
        } 

        return self::$instance;
    }

    
}

?>