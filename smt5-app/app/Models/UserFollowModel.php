<?php

namespace App\Models;

use CodeIgniter\Model;

class UserFollowModel extends Model{
    protected $table = "user_follow";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var UserFollowModel
    */
    private static $instance = null;

    public static function getInstance() : UserFollowModel
    {
        if (self::$instance == null) {
            self::$instance = new UserFollowModel();
        } 

        return self::$instance;
    }
}

?>