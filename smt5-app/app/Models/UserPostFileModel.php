<?php

namespace App\Models;

use CodeIgniter\Model;

class UserPostFileModel extends Model{
    protected $table = "user_post_files";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var UserPostFileModel
    */
    private static $instance = null;

    public static function getInstance() : UserPostFileModel
    {
        if (self::$instance == null) {
            self::$instance = new UserPostFileModel();
        } 

        return self::$instance;
    }
}

?>