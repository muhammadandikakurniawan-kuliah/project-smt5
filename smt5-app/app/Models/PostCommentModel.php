<?php

namespace App\Models;

use CodeIgniter\Model;

class PostCommentModel extends Model{
    protected $table = "post_comments";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var PostCommentModel
    */
    private static $instance = null;

    public static function getInstance() : PostCommentModel
    {
        if (self::$instance == null) {
            self::$instance = new PostCommentModel();
        } 

        return self::$instance;
    }
}

?>