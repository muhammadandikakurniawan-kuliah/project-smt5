<?php

namespace App\Models;

use CodeIgniter\Model;

class UserPostModel extends Model{
    protected $table = "user_posts";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var UserPostModel
    */
    private static $instance = null;

    public static function getInstance() : UserPostModel
    {
        if (self::$instance == null) {
            self::$instance = new UserPostModel();
        } 

        return self::$instance;
    }
}

?>