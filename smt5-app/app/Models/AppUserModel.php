<?php

namespace App\Models;

use CodeIgniter\Model;

class AppUserModel extends Model{
    protected $table = "app_users";
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $protectFields = false;

    /**
     * @var AppUserModel
    */
    private static $instance = null;

    public static function getInstance() : AppUserModel
    {
        if (self::$instance == null) {
            self::$instance = new AppUserModel();
        } 

        return self::$instance;
    }
}

?>