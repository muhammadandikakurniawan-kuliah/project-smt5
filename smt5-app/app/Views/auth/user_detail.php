<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <?= $this->include('post/import_froala_css') ?>
    <link href="<?php echo base_url('css/auth/user_detail.css'); ?>" rel="stylesheet">
    <!-- <link href="<?php echo base_url('css/home/index.css'); ?>" rel="stylesheet"> -->

</head>

<body id="page-top">
<div class="loading-screen d-none">

</div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->

                <?= $this->include('layout/alert') ?>

                <!-- Begin Page Content -->
                <div class="page-content">
                    <div id="coverPictContainer">
                        <img id="coverPict" src="<?= $accountData->coverPictPath ?>" class="" alt="...">
                    </div>
                    
                    <div id="profilePictContainer">
                        <img id="accountProfilePict" class="" src="<?= $accountData->profilePictPath  ?>"/>
                        <div id="nameContainer">
                            <span id="longName"><?= $accountData->longName ?></span>
                            <span id="username">@<?= $accountData->username ?></span>
                            <div class="follow-data-container btn-group">
                                <button onclick="setFollowDataModal('Followers', true)" type="button" class="btn btn-primary btn-follow-data">Followers <span class="badge bg-danger"><?=$accountData->followersCount ?></span> </button>
                                <button onclick="setFollowDataModal('Following', false)" type="button" class="btn btn-primary btn-follow-data">Following <span class="badge bg-danger"><?=$accountData->followingCount ?></span> </button> </button>
                            </div>
                        </div>
                        <div id="profileMenuContainer">
                            <?php if($userData['uuid'] != $accountData->uuid): ?>
                                <button onclick="followeUserProcess()" id="btn-follow" type="button" class="btn btn-secondary">Follow <i class="fa-solid fa-user-plus"></i></button>
                                <a href="<?= base_url('/chat/direct_message/'.$accountData->uuid) ?>" class="btn btn-primary">Chat <i class="fa-solid fa-message"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div id="listPostContainer">
                    <?php foreach($listPost as $post): ?>
                            <div class="post-container card">
                                <div class="post-account-info">

                                    <img class="post-image-profile img-profile rounded-circle" src="<?= $post->user->profilePictPath ?>">
                                    
                                    <span class="post-username"><?= $post->user->username ?></span>
                                    
                                    <span class="created-at-text"><small class="text-muted"><?= $post->createdAt ?></small></span>
                                    <div class="dropdown post-cta-btn">
                                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-black-400"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php if($userData["uuid"] == $post->user->uuid): ?>
                                                <a class="dropdown-item" href="<?= base_url('/post/edit/'.$post->uuid) ?>">Edit</a>
                                                <a class="dropdown-item" onclick="selectDeletePost('<?= $post->uuid ?>')" style="color: red;" href="#" data-toggle="modal" data-target="#confirmationDeleteModal">Delete</a>
                                            <?php endif ?>
                                            <a class="dropdown-item" href="<?= base_url('/post/'.$post->uuid) ?>">Detail</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="carouselExampleIndicators<?=$post->uuid?>" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <?php for($fileIdx = 0; $fileIdx < count($post->files); $fileIdx++): ?>
                                            <?php 
                                                $postFile = $post->files[$fileIdx]; 
                                                $active = $fileIdx == 0 ? "active" : "";
                                            ?>
                                            <li data-target="#carouselExampleIndicators<?=$post->uuid?>" data-slide-to="<?= $fileIdx ?>" class="<?= $active ?>"></li>
                                        <?php endfor ?>
                                    </ol>
                                    <div class="carousel-inner">

                                        <?php for($fileIdx = 0; $fileIdx < count($post->files); $fileIdx++): ?>
                                            <?php 
                                                $postFile = $post->files[$fileIdx]; 
                                                $active = $fileIdx == 0 ? "active" : "";
                                            ?>
                                                <?php if($postFile->isImage) : ?>
                                                    <div class="carousel-item <?= $active ?>">
                                                        <img class="d-block w-100" src="<?= $postFile->filePath ?>">
                                                    </div>
                                                <?php endif ?>

                                                <?php if($postFile->isVideo) : ?>
                                                    <div class="carousel-item <?= $active ?>"><video class="d-block w-100" controls><source src="<?= $postFile->filePath ?>"></video></div>
                                                <?php endif ?>
                                        <?php endfor ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators<?=$post->uuid?>" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators<?=$post->uuid?>" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div class="card-body">
                                <?= $post->textContent ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>


                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?= $this->include('layout/footer') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>

    <!-- Modal followers/following -->
    <div class="modal fade" id="followDataModal" tabindex="-1" aria-labelledby="followDataModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="followDataModalLabel">Modal title</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="userFollowDataContainer">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <!-- End of Page Wrapper -->

    <?= $this->include('post/import_froala_js') ?>

    <script>
        let userLoginUUid = "";
        let targetUserUUID = "";
        let isMe = false;
        let isFollowed = false;
        $(document).ready(() => {
            userLoginUUid = '<?= $userData['uuid'] ?>';
            targetUserUUID = '<?= $accountData->uuid ?>';
            isFollowed = '<?= $accountData->isFollowed ?>';
            isMe = userLoginUUid === targetUserUUID;
            setButtonFollowed(isFollowed);
        })

        function setButtonFollowed(isFollow){
            if(isFollow){
                document.querySelector("#btn-follow").classList.remove('btn-secondary')
                document.querySelector("#btn-follow").classList.add('btn-success')
            }else{
                document.querySelector("#btn-follow").classList.remove('btn-success')
                document.querySelector("#btn-follow").classList.add('btn-secondary')
            }
            
        }

        function setFollowDataModal(title = "", getFollowersData = false){
            document.querySelector("#followDataModalLabel").textContent = title
            $('#followDataModal').modal('toggle');
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/user/get-follow-user') ?>",
                data: JSON.stringify({
                    "userId" : targetUserUUID,
                    "getFollowersData" : getFollowersData,
                }),
                success: (data) =>{
                    if(data['statusCode'] != "USR000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }

                    let users = data['data'] ?  data['data'] : [];

                    let dataContainer = document.querySelector("#userFollowDataContainer")
                    dataContainer.innerHTML = ``;
                    
                    users.forEach((userData) => {
                        dataContainer.innerHTML += `
                            <a class="dropdown-item d-flex align-items-center" href="/user-detail/${userData.uuid}">
                                <div class="mr-3">
                                    <img class="post-image-profile img-profile rounded-circle" src="${userData.profilePictPath}">
                                </div>
                                <div>
                                    <span class="font-weight-bold">${userData.username}</span>
                                </div>
                            </a>
                        `
                    })

                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ follow post error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }

        function followeUserProcess(){
            if(isMe){
                return
            }

            isFollowed = !isFollowed
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/user/follow-user') ?>",
                data: JSON.stringify({
                    "userId" : userLoginUUid,
                    "followedUserId" : targetUserUUID,
                    "follow" : isFollowed,
                }),
                success: (data) =>{
                    if(data['statusCode'] != "USR000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }
                    setButtonFollowed(isFollowed);
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ follow post error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }
    </script>

</body>

</html>