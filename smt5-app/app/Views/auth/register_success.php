<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP Register</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <link href="<?php echo base_url('css/auth/register.css'); ?>" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Hy <?= $userData->longName ?> Your account registration is successfull</h4>
        <hr/>
        <h4 class="alert-heading">Please check your email <?= $userData->email ?> for activate your account</h4>
    </div>

    <?= $this->include('layout/js_sb_admin') ?>
        
    <script>

    </script>

</body>

</html>