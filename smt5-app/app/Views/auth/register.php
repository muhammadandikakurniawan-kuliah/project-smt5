<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP Register</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <link href="<?php echo base_url('css/auth/register.css'); ?>" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                    <?= $this->include('layout/alert') ?>
                    <br/><br/>
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account</h1>
                            </div>
                            <?php 
                                $defaultCoverPict = getenv('S3_EXTERNAL_ENDPOINT').'/'.getenv('DEFAULT_COVER_PROFILE_PICT_PATH') ;
                                $defaultProfilePict = getenv('S3_EXTERNAL_ENDPOINT').'/'.getenv('DEFAULT_PROFILE_PICT_PATH')
                            ?>
                            <form action="/auth/register" method="post" class="user">
                                <?= csrf_field() ?>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input name="coverPict" class="hidden" type="text" id="img-cover-pict-base64str" value="<?= $requestData["coverPict"] ?? $defaultCoverPict ?>"/>
                                        <input onchange="previewImage(event,'img-cover-pict-preview', 'img-cover-pict-base64str')" class="hidden" type="file" id="img-cover-pict-input-file" accept="image/png, image/jpeg, image/jpg" />
                                        <img onclick="openFile('img-cover-pict-input-file')" id="img-cover-pict-preview" src="<?= $defaultCoverPict ?>" alt="...">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 mb-3 mb-sm-0">
                                        <input name="profilePict"t class="hidden" type="text" id="img-profile-pict-base64str" value="<?= $requestData["profilePict"] ??  $defaultProfilePict ?>"/>
                                        <input onchange="previewImage(event,'img-profile-pict-preview', 'img-profile-pict-base64str')" class="hidden" type="file" id="img-profile-pict-input-file" accept="image/png, image/jpeg, image/jpg" />
                                        <img onclick="openFile('img-profile-pict-input-file')" id="img-profile-pict-preview" src="<?= $defaultProfilePict ?>"  class="rounded mx-auto d-block" alt="...">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="firstName" onkeydown="validateInput()" onkeyup="validateInput()" type="text" class="form-control form-control-user" id="firstName" placeholder="First Name" value="<?= $requestData["firstName"] ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lastName" onkeydown="validateInput()" onkeyup="validateInput()" type="text" class="form-control form-control-user" id="lastName" placeholder="Last Name" value="<?= $requestData["lastName"] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input name="email"  onkeydown="validateInput()" onkeyup="validateInput()" type="email" class="form-control form-control-user" id="email" placeholder="Email Address" value="<?= $requestData["email"] ?>">
                                </div>
                                <div class="form-group">
                                    <input name="username" onkeydown="validateInput()" onkeyup="validateInput()" type="text" class="form-control form-control-user" id="username" placeholder="Username" value="<?= $requestData["username"] ?>">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="password" onkeydown="validateInput()" onkeyup="validateInput()" type="password" class="form-control form-control-user" id="password" placeholder="Password" value="<?= $requestData["password"] ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="confirmationPasword" onkeydown="validateInput()" onkeyup="validateInput()" type="password" class="form-control form-control-user" id="confirmationPasword" placeholder="Confirmation Password" value="<?= $requestData["confirmationPasword"] ?>">
                                    </div>
                                </div>
                                
                                <button type="submit" onclick="submitRegister()" id="submit-register-btn" disabled class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="/auth/login">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?= $this->include('layout/js_sb_admin') ?>
        
    <script>

        $( document ).ready(function() {
            validateInput();
        });

        function openFile(id){
            $(`#${id}`).click()
        }

        function previewImage(e, previewId, inputbase64StrId){
            if(e.target.files.length > 0){
                let file = e.target.files[0];
                var src = URL.createObjectURL(file);

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    $(`#${inputbase64StrId}`)[0].value = reader.result
                    $(`#${previewId}`)[0].src = src;
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
            }
        }

        function getInputData(){
            let profilePict = $("#img-profile-pict-base64str")[0].value ?? '<?= $defaultProfilePict ?>';
            let coverPict = $("#img-cover-pict-base64str")[0].value ?? '<?= $defaultCoverPict ?>';
            let firstName = $("#firstName")[0].value ?? '';
            let lastName = $("#lastName")[0].value ?? '';
            let email = $("#email")[0].value ?? '';
            let username = $("#username")[0].value ?? '';
            let password = $("#password")[0].value ?? '';
            let confirmationPasword = $("#confirmationPasword")[0].value ?? '';

            return {
                    "profilePict" : profilePict,
                    "coverPict" : coverPict,
                    "firstName" : firstName,
                    "lastName" : lastName,
                    "email": email, 
                    "username" : username,
                    "password" : password,
                    "confirmationPasword" : confirmationPasword
            };
        }

        function validateInput(){
           
            let inputData = getInputData();

            let errorValidations = [];

            if(
                !inputData["profilePict"].replaceAll(' ', '')
                || !inputData["coverPict"].replaceAll(' ', '')
                || !inputData["firstName"].replaceAll(' ', '')
                || !inputData["firstName"].replaceAll(' ', '')
                || !inputData["email"].replaceAll(' ', '')
                || !inputData["username"].replaceAll(' ', '')
                || !inputData["password"].replaceAll(' ', '')
                || !inputData["confirmationPasword"].replaceAll(' ', '')
            ){
                $("#submit-register-btn")[0].setAttribute('disabled','disabled')
            }else{
                $("#submit-register-btn")[0].removeAttribute('disabled')
            }
        }


    </script>

</body>

</html>