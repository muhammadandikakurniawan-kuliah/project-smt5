<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <link href="<?php echo base_url('css/chat/direct_message.css'); ?>" rel="stylesheet">
</head>

<body id="page-top">
<div class="loading-screen d-none">

</div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->

                <?= $this->include('layout/alert') ?>

                <!-- Begin Page Content -->
                <div class="page-content">
                    <div id="container">
                        <div class="list-dm-container">
                            <?php foreach($listDm as $dm): ?>
                                <?php
                                    $chatUUID = $dm->receiver->uuid == $userData['uuid'] ? $dm->sender->uuid : $dm->receiver->uuid;
                                    $dmPict = $dm->receiver->uuid == $userData['uuid'] ? $dm->sender->profilePictPath : $dm->receiver->profilePictPath;
                                    $dmName = $dm->receiver->uuid == $userData['uuid'] ? $dm->sender->username : $dm->receiver->username;
                                ?>
                                <a class="dropdown-item d-flex align-items-center" href="<?= base_url('/chat/direct_message/'.$chatUUID) ?>">
                                <div class="dropdown-list-image mr-3">
                                    <img class="rounded-circle-contact" src="<?= $dmPict ?>">
                                    <div class="status-indicator bg-success"></div>
                                </div>
                                <div class="font-weight-bold">
                                    <div class="text-truncate"><?= $dm->message ?></div>
                                    <div class="small text-gray-500"><?= $dmName ?></div>
                                </div>
                            </a>
                            <?php endforeach ?>
                        </div>
                        <div class="chat-container">
                            <div class="chat-header">
                                <img src="<?= $accountData->profilePictPath ?>" class="account-profile-pict" alt="...">
                                <span class="username"><?= $accountData->longName  ?></span>
                            </div>
                            
                            <div class="list-chat-container">
                                <?php foreach($messages as $message): ?>
                                    <div class="message-container">
                                        <div class="message <?= $message->sender->uuid == $userData['uuid'] ? 'sender-msg' : '' ?>">
                                            <span class="msg-created-at"><?= $message->createdAt ?></span>
                                            <span class="msg-txt"><?= $message->message ?></span>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="input-msg-container">
                                <div contentEditable="true" id="inputMsg" class="input-msg"
                                    onKeyUp="validateInput(event.currentTarget.textContent)"
                                    onKeyPress="validateInput(event.currentTarget.textContent)"
                                ></div>
                                <button onclick="sendMessage()" disabled class="btn-send-msg btn btn-primary btn-user btn-block">
                                    <i class="fa-solid fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <script>
        var conn;
        var userUUID;
        var receiverUUID;
        $(document).ready(() => {
            userUUID = "<?= $userData["uuid"] ?>";
            receiverUUID = "<?= $accountData->uuid ?>";
            var uri = `<?= getenv("WEB_SOCKET_SERVER_ADDRESS") ?>?connection_type=DIRECT_MESSAGE&user_uuid=${userUUID}&receiver_uuid=${receiverUUID}`;
            conn = new WebSocket(uri);
            conn.onopen = function(e) {
                console.log("Connection established!");
                
            };

            conn.onmessage = (msg) => {
                let msgData = JSON.parse(msg.data)
                if(msgData.statusCode != "CHAT000"){
                    setAlert("alert-danger", `Failed send comment, ${msgData.message}`)
                }else{
                    addMessage(msgData.data)
                }
                scrollChatBottom()
            }

            conn.onclose = (d) =>{
                console.log("connection was closed")
                setAlert("alert-danger", `Cannot connect to server`)
            }

            conn.onerror = (err) => {
                console.log("ws error")
                console.log(err)
            }

            scrollChatBottom();
        })

        function validateInput(msg){
            if(msg){
                document.querySelector(".btn-send-msg").removeAttribute('disabled')
            }else{
                document.querySelector(".btn-send-msg").setAttribute('disabled','disabled')
            }
        }

        function setAlert(type, message){
            document.querySelector("#alertsContainer").innerHTML += `
            <div class="alert ${type} alert-dismissible fade show" role="alert">
                <p>${message}</p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>`
        }

        function addMessage(data){
            let senderClass = data.sender.uuid == userUUID ? 'sender-msg' : '';
            let view = `
                <div class="message-container">
                    <div class="message ${senderClass}">
                        <span class="msg-created-at">${data.createdAt}</span>
                        <span class="msg-txt">${data.message}</span>
                    </div>
                </div>
            `;

            document.querySelector('.list-chat-container').innerHTML += view
        }

        function scrollChatBottom(){
            document.querySelector(".list-chat-container").scrollTo(0, document.querySelector(".list-chat-container").scrollHeight)
        }

        function sendMessage(){
            let msgTxt = document.querySelector("#inputMsg").textContent;
            conn.send(JSON.stringify({
                    "from" : userUUID,
                    "receiver_uuid" : receiverUUID,
                    "message" : msgTxt,
                    "connection_type" : "DIRECT_MESSAGE"
                }))

            document.querySelector("#inputMsg").textContent = "";
            validateInput("");
        }
    </script>

</body>

</html>