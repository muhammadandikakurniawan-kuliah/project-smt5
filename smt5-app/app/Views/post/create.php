<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <?= $this->include('post/import_froala_css') ?>
    <link href="<?php echo base_url('css/post/create.css'); ?>" rel="stylesheet">

</head>

<body id="page-top">
<div class="loading-screen d-none">

</div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->

                <?= $this->include('layout/alert') ?>

                <!-- Begin Page Content -->
                <div class="page-content">

                    <div class="input-post-container">
                        <form action="/post/create" method="post" enctype="multipart/form-data">
                            <input type="file" name="postFiles[]" multiple  onchange="previewImage(event)"class="form-control" id="inputFile" accept="image/png, image/jpeg, image/jpg, video/mp4">

                            <input name="textContent" type="text" class="d-none" id="postCaption" aria-describedby="caption"> 
                            <div class="input-form" id="captionEditor"></div>
                            <br/>
                            <button onclick="submitPost()" type="submit" class="submit-post-btn btn btn-primary btn-lg">Submit</button>
                        </form>
                    </div>

                    <div class="preview-post-container">
                        <h3>Post Preview</h3>
                        <hr/>

                        <div id="postFilePreviewContainer" class="carousel slide d-none" data-ride="carousel" data-interval="false">
                            <ol class="carousel-indicators">
                            </ol>
                            <div id="postFilePreview" class="carousel-inner">
                                
                            </div>
                            <a class="carousel-control-prev" href="#postFilePreviewContainer" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#postFilePreviewContainer" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <hr/>
                        <div id="captionView"></div>
                        <br />
                    </div>
                </div>

                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?= $this->include('layout/footer') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <?= $this->include('post/import_froala_js') ?>

    <script>

        $(document).ready(() => {
            (function () {
            new FroalaEditor("#captionEditor", 
                {
                    events: {
                    'contentChanged': function () {
                        var html = this.html.get();
                        var text = JSON.stringify(html);
                        document.querySelector("#postCaption").value = html || "";
                        document.querySelector("#captionView").innerHTML = html;
                    }
                }
                }
            )
            })()
        })

        function showLoadingScreen(isShow){
            if(isShow){
                document.querySelector(".loading-screen").classList.remove("d-none")
            }else{
                document.querySelector(".loading-screen").classList.add("d-none")
            }
        }

        function previewImage(e){
            if(e.target.files.length > 0){

                for(var fileIdx = 0; fileIdx < e.target.files.length; fileIdx++){
                    let file = e.target.files[fileIdx];
                    var src = URL.createObjectURL(file);
                    let fileType = file.type.split("/")[0];
                    let activeClas = fileIdx == 0 ? 'active' : '';
                    let content = ``
                    

                    if(fileType == "image"){
                        content = `<div class="carousel-item ${activeClas}"><img class="d-block w-100" src="${src}" alt="First slide"></div>`
                    }else if(fileType == "video"){
                        content = `<div class="carousel-item ${activeClas}"><video class="d-block w-100" controls><source src="${src}"></video></div>`
                    }

                    $("#postFilePreview")[0].innerHTML += content
                    $(".carousel-indicators")[0].innerHTML += ` <li data-target="#postFilePreviewContainer" data-slide-to="${fileIdx}" class=" ${activeClas}"></li>`
                }
                $("#postFilePreviewContainer")[0].classList.remove("d-none")
            }else{
                $("#postFilePreviewContainer")[0].classList.add("d-none")
            }
        }

        function readFileAsBase64(file){
            return new Promise((resolve, reject) => {
                let reader = new FileReader();
                reader.readAsDataURL(file)
                reader.onload = function () {
                    let res = reader.result;
                    console.log(res)
                    resolve(res.split("base64,")[1])
                }
                reader.onerror = (err) => {
                    reject(err)
                }
            })
        }

        function setAlert(type, message){
            document.querySelector("#alertsContainer").innerHTML += `
                    <div class="alert ${type} alert-dismissible fade show" role="alert">
                        <p>${message}</p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
        }

        function setupListFile(){
            return new Promise((resolve, reject) => {
                let filesRequestData = [];
                let files = document.querySelector("#inputFile").files;

                let timeOut = 10*files.length;

                for(var fileIdx = 0; fileIdx < files.length; fileIdx++){
                    let file = files[fileIdx];
                    let fileDataReq = {
                        "name" : file.name,
                        "type" : file.type
                    }

                    readFileAsBase64(file).then(res => {
                        fileDataReq["base64"] = res 
                        filesRequestData.push(fileDataReq)
                    })
                }
                setTimeout(() => {
                    resolve(filesRequestData);
                }, timeOut);
            })
        }

        function submitPost(){
            // setupListFile()
            // .then((dataFiles) => {
            //     let dataRequest = {
            //         "files": dataFiles || [],
            //         "caption" : document.querySelector("#postCaption").value
            //     };
                
            //     console.log(dataFiles);
                
            showLoadingScreen(true)
            //     $.ajax({
            //         contentType: 'application/json',
            //         type: "POST",
            //         dataType: 'json',
            //         url : "<?= base_url("PostController/createProcess") ?>",
            //         data: JSON.stringify(dataRequest),
            //         success: (data) =>{
            //             console.log("============ success response ==============")
            //             console.log(data)
            //             showLoadingScreen(false)
            //             setAlert("alert-success","Submit Post Success")
                       
            //         },
            //         error : (err) => {
            //             setAlert("alert-danger","internal error")
            //             console.log("================ submit post error ==================")
            //             console.log(err)
            //             showLoadingScreen(false)
            //         }
            //     });
            // })
            // .catch((err) => {
            //     setAlert("alert-danger","internal error")
            //     console.log("================ submit post error ==================")
            //     console.log(err) 
            // })
        }

    </script>

</body>

</html>