<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <?= $this->include('post/import_froala_css') ?>
    <link href="<?php echo base_url('css/post/edit.css'); ?>" rel="stylesheet">
</head>

<body id="page-top">
<div class="loading-screen d-none">

</div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->

                <?= $this->include('layout/alert') ?>

                <!-- Begin Page Content -->
                <div class="page-content">
                    <form action="<?= base_url('post/edit/'.$postData->uuid) ?>" method="post">
                        <button onclick="submitPost()" id="submitBtn" type="submit" class="btn btn-primary">Save <i class="fa-solid fa-floppy-disk"></i></button>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                            <ol class="carousel-indicators">
                                <?php for($fileIdx = 0; $fileIdx < count($postData->files); $fileIdx++): ?>
                                    <?php 
                                        $postFile = $postData->files[$fileIdx]; 
                                        $active = $fileIdx == 0 ? "active" : "";
                                    ?>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $fileIdx ?>" class="<?= $active ?>"></li>
                                <?php endfor ?>
                            </ol>
                            <div class="carousel-inner">

                                <?php for($fileIdx = 0; $fileIdx < count($postData->files); $fileIdx++): ?>
                                    <?php 
                                        $postFile = $postData->files[$fileIdx]; 
                                        $active = $fileIdx == 0 ? "active" : "";
                                    ?>
                                        <?php if($postFile->isImage) : ?>
                                            <div class="carousel-item <?= $active ?>">
                                                <img class="d-block w-100" src="<?= $postFile->filePath ?>">
                                            </div>
                                        <?php endif ?>

                                        <?php if($postFile->isVideo) : ?>
                                            <div class="carousel-item <?= $active ?>"><video class="d-block w-100" controls><source src="<?= $postFile->filePath ?>"></video></div>
                                        <?php endif ?>
                                <?php endfor ?>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                        <div id="textContentContainer">
                            <input name="textContent" type="text" class="d-none" id="postCaption" aria-describedby="caption"> 
                            <div name="textContent" class="input-form" id="captionEditor">
                                <?= $postData->textContent ?>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?= $this->include('layout/footer') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <?= $this->include('post/import_froala_js') ?>
    <script>
       $(document).ready(() => {
            (function () {
            new FroalaEditor("#captionEditor", 
                {
                    events: {
                    'contentChanged': function () {
                        var html = this.html.get();
                        var text = JSON.stringify(html);
                        document.querySelector("#postCaption").value = html;
                    },
                    'html.set': function () {
                        var html = this.html.get();
                        document.querySelector("#postCaption").value = html;
                    }
                }
                }
            )
            })()
        })

        function showLoadingScreen(isShow){
            if(isShow){
                document.querySelector(".loading-screen").classList.remove("d-none")
            }else{
                document.querySelector(".loading-screen").classList.add("d-none")
            }
        }

        function submitPost(){
            showLoadingScreen(true)
        }
    </script>

</body>

</html>