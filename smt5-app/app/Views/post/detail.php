<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <link href="<?php echo base_url('css/post/detail.css'); ?>" rel="stylesheet">
</head>

<body id="page-top">
<div class="loading-screen d-none">

</div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->

                <?= $this->include('layout/alert') ?>
                <!-- Begin Page Content -->
                <div class="page-content">
                    <div class="uploader-data">
                        <img class="post-image-profile img-profile rounded-circle" src="<?= $postData->user->profilePictPath ?>">
                        <a href="<?= base_url('/user-detail/'.$postData->user->uuid) ?>" class="post-username"><?= $postData->user->username ?></a>
                    </div>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                        <ol class="carousel-indicators">
                            <?php for($fileIdx = 0; $fileIdx < count($postData->files); $fileIdx++): ?>
                                <?php 
                                    $postFile = $postData->files[$fileIdx]; 
                                    $active = $fileIdx == 0 ? "active" : "";
                                ?>
                                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $fileIdx ?>" class="<?= $active ?>"></li>
                            <?php endfor ?>
                        </ol>
                        <div class="carousel-inner">

                            <?php for($fileIdx = 0; $fileIdx < count($postData->files); $fileIdx++): ?>
                                <?php 
                                    $postFile = $postData->files[$fileIdx]; 
                                    $active = $fileIdx == 0 ? "active" : "";
                                ?>
                                    <?php if($postFile->isImage) : ?>
                                        <div class="carousel-item <?= $active ?>">
                                            <img class="d-block w-100" src="<?= $postFile->filePath ?>">
                                        </div>
                                    <?php endif ?>

                                    <?php if($postFile->isVideo) : ?>
                                        <div class="carousel-item <?= $active ?>"><video class="d-block w-100" controls><source src="<?= $postFile->filePath ?>"></video></div>
                                    <?php endif ?>
                            <?php endfor ?>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div id="textContent">
                        <?= $postData->textContent ?>
                    </div>

                    <hr />
                    
                    <div id="commnetContainer">
                        <h5>Commentar</h5>
                        <div id="inputCommentContainer">
                            <textarea onkeyup="isAbleToSendComment(event.target.value)" onkeydown="isAbleToSendComment(event.target.value)" id="inputComment"></textarea>
                            <button onclick="sendComment()" id="commentBtn" disabled type="button" class="btn btn-primary">
                                <i class="fa-solid fa-paper-plane"></i>
                            </button>
                        </div>
                        <hr/>
                        <div id="listCommentContainer">
                            <?php foreach($postData->comments as $comment): ?>
                                <div class="comment-container">
                                    <img class="post-image-profile img-profile rounded-circle" src="<?= $comment->user->profilePictPath ?>">
                                    <div class="comment-info">
                                        <span><b><?= $comment->user->username ?></b></span>
                                        <span><?= $comment->createdAt ?></span>
                                        <br/>
                                        <p><?= $comment->commentTxt ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <button onclick="retrivePostComment()" id="showMoreCommentBtn" type="button" class="btn btn-dark">Show More Comment</button>
                    </div>
    
                </div>
                
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?= $this->include('layout/footer') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->
    <script>
        var conn;
        var userUUID;
        var postUUID;
        var commentPage = 2;
        var commentLength = 5;
        $(document).ready(() => {
            userUUID = "<?= $userData["uuid"] ?>";
            postUUID = "<?= $postData->uuid ?>";
            var uri = `<?= getenv("WEB_SOCKET_SERVER_ADDRESS") ?>?connection_type=POST_COMMENT&user_uuid=${userUUID}&post_uuid=${postUUID}`;
            conn = new WebSocket(uri);
            conn.onopen = function(e) {
                console.log("Connection established!");
                
            };

            conn.onmessage = (msg) => {
                let msgData = JSON.parse(msg.data)
                if(msgData.statusCode != "POST000"){
                    setAlert("alert-danger", `Failed send comment, ${msgData.message}`)
                }else{
                    addCommentView(msgData.data)
                }
                
            }

            conn.onclose = (d) =>{
                console.log("connection was closed")
                setAlert("alert-danger", `Cannot connect to server`)
            }

            conn.onerror = (err) => {
                console.log("ws error")
                console.log(err)
            }
        })

        function setAlert(type, message){
            document.querySelector("#alertsContainer").innerHTML += `
                    <div class="alert ${type} alert-dismissible fade show" role="alert">
                        <p>${message}</p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
        }

        function addCommentView(data, isIncrement){
            let template = `<div class="comment-container">
                <img class="post-image-profile img-profile rounded-circle" src="${data.user.profilePictPath}">
                <div class="comment-info">
                    <span><b>${data.user.username}</b></span>
                    <span>${data.createdAt}</span>
                    <br/>
                    <p>${data.commentTxt}</p>
                </div>
            </div>`

            let currentChats = document.querySelector("#listCommentContainer").innerHTML;
            

            if(isIncrement){
                currentChats += template
            }else{
                currentChats = template+currentChats
            }

            document.querySelector("#listCommentContainer").innerHTML = currentChats;
        }

        function isAbleToSendComment(commentTxt){
            if(!commentTxt){
                document.querySelector("#commentBtn").setAttribute('disabled','')
            }else{
                document.querySelector("#commentBtn").removeAttribute('disabled')
            }
        }

        function sendComment(){
            let commentTxt = document.querySelector("#inputComment").value;
            conn.send(JSON.stringify({
                    "from" : userUUID,
                    "post_uuid" : postUUID,
                    "message" : commentTxt,
                    "connection_type" : "POST_COMMENT"
                }))

            document.querySelector("#inputComment").value = "";
            isAbleToSendComment("");
        }

        function retrivePostComment(){
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/post/get-comments') ?>",
                data: JSON.stringify({
                    "post_uuid": postUUID,
                    "comment_page": commentPage,
                    "comment_length" : commentLength
                }),
                success: (data) =>{
                    if(data['statusCode'] != "POST000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }

                    let dataPost = data.data[0] ?? {'comments':[]};

                    data.data[0].comments.forEach((commentData) => {
                        addCommentView(commentData, true);
                    })

                    
                    commentPage++
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ submit post error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }
        
    </script>

</body>

</html>