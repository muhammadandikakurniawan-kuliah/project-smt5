
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/froala_editor.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/froala_style.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/code_view.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/colors.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/emoticons.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/image_manager.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/image.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/line_breaker.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/table.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/char_counter.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('/froala_editor/css/plugins/video.css'); ?>">