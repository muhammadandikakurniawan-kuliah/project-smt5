<!-- Custom fonts for this template-->
<link href="<?php echo base_url('sb-admin-2/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->
<link href="<?php echo base_url('sb-admin-2/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('bootstrap-5.0.2/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('css/layout/top_nav_bar.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('css/layout/alert.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('fontawesome-free-6.2.0/css/all.min.css'); ?>" rel="stylesheet">