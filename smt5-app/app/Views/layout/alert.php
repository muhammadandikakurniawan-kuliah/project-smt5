<div id="alertsContainer">
<?php foreach($alerts as $alert): ?>
    <div class="alert <?= $alert['type'] ?> alert-dismissible fade show" role="alert">
        <p><?= $alert['message'] ?></p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php endforeach;  ?>
</div>