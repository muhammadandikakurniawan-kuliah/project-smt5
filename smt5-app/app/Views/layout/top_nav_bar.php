  <!-- Topbar -->
  <nav class="top-nav-bar navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

<!-- Sidebar Toggle (Topbar) -->
<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
</button>


<a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
    <div class="sidebar-brand-icon">
        <i class="fas fa-house"></i>
    </div>
</a>

<!-- Topbar Search -->
<form
    class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
    <div class="input-group">
        <input id="searchKeyword" type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
            aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
            <button onclick="searchProcess(null)" class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
            </button>
        </div>
    </div>
</form>

<!-- Dropdown - Alerts -->
<div id="searchResultContainer" class="dropdown-list dropdown-menu shadow animated--grow-in" aria-labelledby="alertsDropdown">

</div>


<!-- Button Create Post -->
<a href="<?= base_url('post/create') ?>" id="create-post-btn" type="button" class="btn btn-primary">
    Create Post <i style="margin-left: 10px;" class="fa fa-plus"></i>
</a>

<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">
    

    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
    <li class="nav-item dropdown no-arrow d-sm-none">
        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
            aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small"
                        placeholder="Search for..." aria-label="Search"
                        aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </li>

    
    <!-- Nav Item - Alerts -->
    <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            <span class="badge badge-danger badge-counter">3+</span>
        </a>
        
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
            aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">
                Alerts Center
            </h6>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-success">
                        <i class="fas fa-donate text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                </div>
            </a>
            <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="mr-3">
                    <div class="icon-circle bg-warning">
                        <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                </div>
            </a>
            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
        </div>
    </li>

    <!-- Nav Item - Messages -->
    <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <!-- Counter - Messages -->
            <span id="messageCenterBadgeNotif" class="d-none badge badge-danger badge-counter">
                <i class="fa-solid fa-bell"></i>
            </span>
        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
            aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">
                Message Center
            </h6>
            <div id="listMessageCenterContainer">
                
                
            </div>
            
            <a class="dropdown-item text-center small text-gray-500" href="<?= base_url('/chat/direct_message')?>">Read More Messages</a>
        </div>
    </li>

    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $userData["username"] ?></span>
            <img class="img-profile rounded-circle" src="<?= $userData["profilePictPath"]  ?>">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
            aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?= base_url('/user-detail/'.$userData['uuid']) ?>">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profile
            </a>
            <a class="dropdown-item" href="#">
                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                Settings
            </a>
            <a class="dropdown-item" href="#">
                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= base_url("/logout") ?>">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
            </a>
        </div>
    </li>

</ul>

</nav>
<!-- End of Topbar -->
<hr/>
<hr/>
<hr/>
<hr/>

<?= $this->include('layout/js_sb_admin') ?>

<script>
    var conn;
    var userUUID;
    $(document).ready(() => {
        getLatestDm()
        userUUID = "<?= $userData["uuid"] ?>";
        var uri = `<?= getenv("WEB_SOCKET_SERVER_ADDRESS") ?>?connection_type=NOTIF_DM&user_uuid=${userUUID}`;
        conn = new WebSocket(uri);
        conn.onopen = function(e) {
            console.log("Connection established!");
        };

        conn.onmessage = (msg) => {
            let msgData = JSON.parse(msg.data)
            if(msgData.statusCode == "CHAT000"){
                console.log("==================== notif dm ===================")
                console.log(msgData)
                setMessageCenterElement(msgData.data)
                document.querySelector('#messageCenterBadgeNotif').classList.remove('d-none')
            }
        }

        conn.onclose = (d) =>{
            console.log("connection was closed")
            setAlert("alert-danger", `Cannot connect to server`)
        }

        conn.onerror = (err) => {
            console.log("ws error")
            console.log(err)
        }
    })


    function getLatestDm(){
        $.ajax({
            contentType: 'application/json',
            type: "POST",
            dataType: 'json',
            url : "<?= base_url('/chat/latest-dm') ?>",
            data: JSON.stringify({
                "perPage" : 5,
                'page':1
            }),
            success: (data) =>{
                console.log(data)
                if(data.statusCode != "CHAT000"){
                    setAlert("alert-danger","internal error")
                    console.log("==================== failed get dm ===================")
                    console.log(data)
                    return    
                }
                
                setMessageCenterElement(data.data)
            },
            error : (err) => {
                setAlert("alert-danger","internal error")
                console.log("================ get dm error ==================")
                console.log(err)
            }
        });
    }

    function showLoadingScreen(isShow){
        if(isShow){
            document.querySelector(".loading-screen").classList.remove("d-none")
        }else{
            document.querySelector(".loading-screen").classList.add("d-none")
        }
    }

    function selectDeletePost(uuid){
        document.querySelector("#deleteProcessBtn").setAttribute("data-post-uuid", uuid);
    }

    function setAlert(type, message){
        document.querySelector("#alertsContainer").innerHTML += `
                <div class="alert ${type} alert-dismissible fade show" role="alert">
                    <p>${message}</p>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>`
    }

    function searchTag(searchKey, okCallback){
        $.ajax({
            contentType: 'application/json',
            type: "POST",
            dataType: 'json',
            url : "<?= base_url('/post/search-tag') ?>",
            data: JSON.stringify({
                "searchKey": searchKey,
                "perPage" : 10
            }),
            success: (data) =>{
                if(data['statusCode'] != "POST000"){
                    setAlert("alert-danger","internal error") 
                    showLoadingScreen(false)
                    return
                }

                okCallback(data);
                showLoadingScreen(false)
                
            },
            error : (err) => {
                setAlert("alert-danger","internal error")
                console.log("================ search tag error ==================")
                console.log(err)
                showLoadingScreen(false)
            }
        });
    }

    function searchUser(searchKey, okCallback){
        $.ajax({
            contentType: 'application/json',
            type: "POST",
            dataType: 'json',
            url : "<?= base_url('/user/search') ?>",
            data: JSON.stringify({
                "searchKey": searchKey,
                "perPage" : 10
            }),
            success: (data) =>{
                if(data['statusCode'] != "AUTH000"){
                    setAlert("alert-danger","internal error") 
                    showLoadingScreen(false)
                    return
                }

                okCallback(data);
                showLoadingScreen(false)
                
            },
            error : (err) => {
                setAlert("alert-danger","internal error")
                console.log("================ search user error ==================")
                console.log(err)
                showLoadingScreen(false)
            }
        });
    }

    function searchProcess(key){
        let searchKey = '';
        if(key){
            searchKey = key
        }else{
            searchKey = document.querySelector("#searchKeyword").value;
        }

        document.querySelector("#searchResultContainer").classList.add("show")


        var searchResultContainer = document.querySelector("#searchResultContainer");
        searchResultContainer.innerHTML = ``;
        searchUser(searchKey, (data) => {
            console.log("====================== search user ===================")
            console.log(data)
            data.data.forEach((userData) => {
                searchResultContainer.innerHTML += `
                <a class="dropdown-item d-flex align-items-center" href="/user-detail/${userData.uuid}">
                    <div class="mr-3">
                        <img class="post-image-profile img-profile rounded-circle" src="${userData.profilePictPath}">
                    </div>
                    <div>
                        <span class="font-weight-bold">${userData.username}</span>
                    </div>
                </a>
                `
            })
        });

        searchTag(searchKey, (data) => {
            console.log("====================== search tag ===================")
            console.log(data)
            data.data.forEach((tag) => {
                searchResultContainer.innerHTML += `
                <a class="dropdown-item d-flex align-items-center" href="/?tag=${tag}">
                    <div>
                        <span class="font-weight-bold">#${tag}</span>
                    </div>
                </a>
                `
            })
        });
        // console.log(searchKey);
    }

    function messageCenterElementComponent(data){
        let component = `<a class="dropdown-item d-flex align-items-center" href="<?= base_url('/chat/direct_message/') ?>/${data['uuid']}">
            <div class="dropdown-list-image mr-3">
                <img class="rounded-circle" src="${data['profilePictPath']}">
                <div class="status-indicator bg-success"></div>
            </div>
            <div class="font-weight-bold">
                <div class="text-truncate">${data['message']}</div>
                <div class="small text-gray-500">${data['username']}</div>
            </div>
        </a>`

        return component
    }

    function setMessageCenterElement(datas){
        if(!datas || datas.length <= 0){
            return
        }
        document.querySelector('#listMessageCenterContainer').innerHTML = ``
        datas.forEach(data => {
            let dataComponent = data.sender;
            if(dataComponent.uuid == userUUID){
                dataComponent = data.receiver
            }
            dataComponent.message = data.message
            document.querySelector('#listMessageCenterContainer').innerHTML += messageCenterElementComponent(dataComponent)
        })
    }

</script>