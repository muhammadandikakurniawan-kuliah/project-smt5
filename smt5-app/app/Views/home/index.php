<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KELOMPOK5APP</title>

    <!-- Custom fonts for this template-->
    <?= $this->include('layout/css_sb_admin') ?>
    <link href="<?php echo base_url('css/home/index.css'); ?>" rel="stylesheet">

</head>

<body id="page-top">
<div class="loading-screen d-none"> </div>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?= $this->include('layout/top_nav_bar') ?>
                <!-- End of Topbar -->
                <?= $this->include('layout/alert') ?>
                <!-- Begin Page Content -->
                <div class="page-content row">
                    <div class="list-post-container col-8">
                        <?php foreach($listPost as $post): ?>
                            <div class="post-container card">
                                <div class="post-account-info">

                                    <img class="post-image-profile img-profile rounded-circle" src="<?= $post->user->profilePictPath ?>">
                                    
                                    <a href="<?= base_url('/user-detail/'.$post->user->uuid) ?>" class="post-username"><?= $post->user->username ?></a>
                                    
                                    <span class="created-at-text"><small class="text-muted"><?= $post->createdAt ?></small></span>
                                    <div class="dropdown post-cta-btn">
                                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-black-400"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php if($userData["uuid"] == $post->user->uuid): ?>
                                                <a class="dropdown-item" href="<?= base_url('/post/edit/'.$post->uuid) ?>">Edit</a>
                                                <a class="dropdown-item" onclick="selectDeletePost('<?= $post->uuid ?>')" style="color: red;" href="#" data-toggle="modal" data-target="#confirmationDeleteModal">Delete</a>
                                            <?php endif ?>
                                            <a class="dropdown-item" href="<?= base_url('/post/'.$post->uuid) ?>">Detail</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="carouselExampleIndicators<?=$post->uuid?>" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <ol class="carousel-indicators">
                                        <?php for($fileIdx = 0; $fileIdx < count($post->files); $fileIdx++): ?>
                                            <?php 
                                                $postFile = $post->files[$fileIdx]; 
                                                $active = $fileIdx == 0 ? "active" : "";
                                            ?>
                                            <li data-target="#carouselExampleIndicators<?=$post->uuid?>" data-slide-to="<?= $fileIdx ?>" class="<?= $active ?>"></li>
                                        <?php endfor ?>
                                    </ol>
                                    <div class="carousel-inner">

                                        <?php for($fileIdx = 0; $fileIdx < count($post->files); $fileIdx++): ?>
                                            <?php 
                                                $postFile = $post->files[$fileIdx]; 
                                                $active = $fileIdx == 0 ? "active" : "";
                                            ?>
                                                <?php if($postFile->isImage) : ?>
                                                    <div class="carousel-item <?= $active ?>">
                                                        <img class="d-block w-100" src="<?= $postFile->filePath ?>">
                                                    </div>
                                                <?php endif ?>

                                                <?php if($postFile->isVideo) : ?>
                                                    <div class="carousel-item <?= $active ?>"><video class="d-block w-100" controls><source src="<?= $postFile->filePath ?>"></video></div>
                                                <?php endif ?>
                                        <?php endfor ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators<?=$post->uuid?>" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators<?=$post->uuid?>" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div class="card-body">
                                <?= $post->textContent ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <div class="trending-container col-4">
                        <h1 class="title-trending">Trending</h1>
                        <div class="list-tag-container">
                            <?php foreach($trendingData as $data) : ?>
                                <div class="tag-container">
                                   <a class="link-tag" href="<?= $data->link ?>"> <?= $data->tag ?> </a>
                                   <span class="count-post"><?= $data->count ?> Posts</span>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?= $this->include('layout/footer') ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Confirm Delete Modal -->
    <div class="modal fade" id="confirmationDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmationDeleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmationDeleteModalLabel">Delete Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to delete this post ?
            </div>
            <div class="modal-footer">
                <button data-post-uuid="" id="deleteProcessBtn" type="button" onclick="deleteProcess(event)" class="btn btn-danger">Delete</button>
            </div>
            </div>
        </div>
    </div>

    <script>
        var currentPostPage = 2;
        var perpage = 10;
        var tag = `<?= $tag ?>`
        $(document).ready(() => {
            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() == $(document).height()) {
                    retrivePost(tag,'','post',currentPostPage,perpage, true)
                    currentPostPage++
                }
            });
        })

        function showLoadingScreen(isShow){
            if(isShow){
                document.querySelector(".loading-screen").classList.remove("d-none")
            }else{
                document.querySelector(".loading-screen").classList.add("d-none")
            }
        }

        function selectDeletePost(uuid){
            document.querySelector("#deleteProcessBtn").setAttribute("data-post-uuid", uuid);
        }

        function setAlert(type, message){
            document.querySelector("#alertsContainer").innerHTML += `
                    <div class="alert ${type} alert-dismissible fade show" role="alert">
                        <p>${message}</p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
        }

        function postComponent(postData){
            var result = `<div class="post-container card">
                <div class="post-account-info">
                    <a href="<?= base_url('/user-detail/') ?>${postData.user.uuid}" class="post-username">${postData.user.username}</a>
                    <img class="post-image-profile img-profile rounded-circle" src="${postData.user.profilePictPath}">
                    <span class="created-at-text"><small class="text-muted">${postData.createdAt}</small></span>
                    <div class="dropdown post-cta-btn">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-list fa-sm fa-fw mr-2 text-black-400"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">`

            if(postData.user.uuid == '<?= $userData["uuid"] ?>'){
                result += `
                <a class="dropdown-item" href="<?= base_url('/post/edit/') ?>${postData.uuid}">Edit</a>
                <a class="dropdown-item" onclick="selectDeletePost('${postData.uuid}')" style="color: red;" href="#" data-toggle="modal" data-target="#confirmationDeleteModal">Delete</a>
                `
            }

            result += `<a class="dropdown-item" href="<?= base_url('/post/') ?>${postData.uuid}">Detail</a>
                        </div>
                    </div>
                </div>
                <div id="carouselExampleIndicators${postData.uuid}" class="carousel slide" data-ride="carousel" data-interval="false">
                    <ol class="carousel-indicators">`

            result += postData.files.map((postFile, idx) => {
                let active = idx == 0 ? "active" : "";
                return `<li data-target="#carouselExampleIndicators${postFile.uuid}" data-slide-to="${idx}" class="${active}"></li>`
            })
                    

            result += `</ol>
            <div class="carousel-inner">`

            result += postData.files.map((postFile, idx) => {
                let active = idx == 0 ? "active" : "";
                let viewRes = ''
                if(postFile.isImage){
                    viewRes += `<div class="carousel-item ${active}">
                        <img class="d-block w-100" src="${postFile.filePath}">
                    </div>
                `
                }
                if(postFile.isVideo){
                    viewRes += `<div class="carousel-item ${active}">
                        <video class="d-block w-100" controls><source src="${postFile.filePath}"></video>
                    </div>`
                }

                return viewRes;
            })

            result += `</div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators${postData.uuid}" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators${postData.uuid}" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="card-body">
                ${postData.textContent}
                </div>
            </div>`

            return result;
        }

        function setDataPostView(listPost, increment){
            if(!increment){
                document.querySelector(".list-post-container").innerHTML = ''
            }
            listPost.forEach((el, idx) => {
                let view = postComponent(el);
                document.querySelector(".list-post-container").innerHTML += view;
            })
        }

        function retrivePost(tag, searchKey, searchType ,page, limit, increment){
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/post/search') ?>",
                data: JSON.stringify({
                    "tag": tag,
                    "searchKey": searchKey,
                    "searchType": searchType,
                    "perPage" : limit,
                    "page": page
                }),
                success: (data) =>{
                    if(data['statusCode'] != "POST000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }
                    setDataPostView(data.data, increment)
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ submit post error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }

        function deleteProcess(e){
            showLoadingScreen(true);
            let postUUID = e.target.getAttribute("data-post-uuid");
            $.ajax({
                contentType: 'application/json',
                type: "DELETE",
                dataType: 'json',
                url : "<?= base_url('PostController/deleteProcess') ?>",
                data: JSON.stringify({
                    "post_uuid" : postUUID
                }),
                success: (data) =>{
                    if(data['statusCode'] != "POST000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }
                    retrivePost(tag,'','post',1,perpage, false)
                    showLoadingScreen(false)
                    setAlert("alert-success","Delete Post Success")
                    
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ Delete post error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }

        function searchTag(searchKey, okCallback){
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/post/search-tag') ?>",
                data: JSON.stringify({
                    "searchKey": searchKey,
                    "perPage" : 10
                }),
                success: (data) =>{
                    if(data['statusCode'] != "POST000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }

                    okCallback(data);
                    showLoadingScreen(false)
                    
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ search tag error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }

        function searchUser(searchKey, okCallback){
            $.ajax({
                contentType: 'application/json',
                type: "POST",
                dataType: 'json',
                url : "<?= base_url('/user/search') ?>",
                data: JSON.stringify({
                    "searchKey": searchKey,
                    "perPage" : 10
                }),
                success: (data) =>{
                    if(data['statusCode'] != "AUTH000"){
                        setAlert("alert-danger","internal error") 
                        showLoadingScreen(false)
                        return
                    }

                    okCallback(data);
                    showLoadingScreen(false)
                    
                },
                error : (err) => {
                    setAlert("alert-danger","internal error")
                    console.log("================ search user error ==================")
                    console.log(err)
                    showLoadingScreen(false)
                }
            });
        }

        function searchProcess(key){
            let searchKey = '';
            if(key){
                searchKey = key
            }else{
                searchKey = document.querySelector("#searchKeyword").value;
            }

            document.querySelector("#searchResultContainer").classList.add("show")


            var searchResultContainer = document.querySelector("#searchResultContainer");
            searchResultContainer.innerHTML = ``;
            searchUser(searchKey, (data) => {
                console.log("====================== search user ===================")
                console.log(data)
                data.data.forEach((userData) => {
                    searchResultContainer.innerHTML += `
                    <a class="dropdown-item d-flex align-items-center" href="/user-detail/${userData.uuid}">
                        <div class="mr-3">
                            <img class="post-image-profile img-profile rounded-circle" src="${userData.profilePictPath}">
                        </div>
                        <div>
                            <span class="font-weight-bold">${userData.username}</span>
                        </div>
                    </a>
                    `
                })
            });

            searchTag(searchKey, (data) => {
                console.log("====================== search tag ===================")
                console.log(data)
                data.data.forEach((tag) => {
                    searchResultContainer.innerHTML += `
                    <a class="dropdown-item d-flex align-items-center" href="/?tag=${tag}">
                        <div>
                            <span class="font-weight-bold">#${tag}</span>
                        </div>
                    </a>
                    `
                })
            });
            // console.log(searchKey);
        }

    </script>

</body>

</html>