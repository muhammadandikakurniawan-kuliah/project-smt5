<?php
namespace App\Database\MongoDb;

use Exception;
use \MongoDB\Client as MongoDbClient;

class Connection {

    /**
     * @var string
    */
    private $uri;

    /**
     * @var string
    */
    private $dbName;

    /**
     * @var MongoDbClient
    */
    private $client;

    /**
     * @var Connection
    */
    private static $instance = null;

    public static function  getInstance() : Connection{
        if(self::$instance == null){
            self::$instance = new Connection(); 
        }
        return self::$instance;
    }

    function __construct()
    {
        try{
            $this->uri = getenv("MONGODB_URI");
            $this->dbName = getenv("MONGODB_DBNAME");
            $this->client = new MongoDbClient($this->uri);
        }catch(Exception $ex){
            dd('Couldn\'t connect to database: ' . $ex->getMessage(), 500);
        }
    }

    /**
     * @return MongoDbClient
    */
    public function getClient()
    {
        return $this->client;
    }
}

?>