<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;


class NoLoginFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $userData = session()->get("USER_DATA");
    	if ($userData)
	    {
	        return redirect()->to(base_url('/'));
	    }
        // Do something here
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}



// public function before(RequestInterface $request, $arguments = null);

// /**
//  * Allows After filters to inspect and modify the response
//  * object as needed. This method does not allow any way
//  * to stop execution of other after filters, short of
//  * throwing an Exception or Error.
//  *
//  * @param null $arguments
//  *
//  * @return mixed
//  */
// public function after(RequestInterface $request, ResponseInterface $response, $arguments = null);