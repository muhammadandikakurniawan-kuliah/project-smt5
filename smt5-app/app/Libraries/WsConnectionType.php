<?php 
namespace App\Libraries;

class WsConnectionType {
    public const DIRECT_MESSAGE = "DIRECT_MESSAGE";
    public const POST_COMMENT = "POST_COMMENT";
    public const NOTIF_DM = "NOTIF_DM";
}

?>