<?php 
namespace App\Libraries;

use Exception;
use Ratchet\ConnectionInterface;

class WsAppUserConnectionStorage {

    /**
     * @var ConnectionInterface[][]
    */
    private $connectionsByUserUUID;

    /**
     * @var ConnectionInterface[][][]
    */
    private $connectionsByPostUUID;

    /**
     * @var ConnectionInterface[][][]
    */
    private $connectionDirectMessageByUserUUID;

    /**
     * @var ConnectionInterface[][]
    */
    private $connectionNotifDirectMessageByUserUUID;

    public function __construct()
    {
        /**
         * @var ConnectionInterface[][]
        */
        $this->connectionsByUserUUID = [];

        /**
         * @var ConnectionInterface[][][]
        */
        $this->connectionsByPostUUID = [];

        /**
         * @var ConnectionInterface[][]
        */
        $this->connectionChatByUserUUID = [];
    }

    /**
     * @return ConnectionInterface[]
    */
    public function addConnectionByUserUUID(string $uuid, ConnectionInterface $conn) : array{
        if(empty($this->connectionsByUserUUID[$uuid])){
            $this->connectionsByUserUUID[$uuid] = [$conn];
        }else{
            array_push($this->connectionsByUserUUID[$uuid], $conn);
        }
        return $this->getConnectionByUserUUID($uuid);
    }

    /**
     * @return ConnectionInterface[]
    */
    public function getConnectionByUserUUID(string $uuid) : array{
        return $this->connectionsByUserUUID[$uuid] ?? [];
    }

    public function addConnectionByPostUUID(string $postuuid, string $userUuid, ConnectionInterface $conn) : ConnectionInterface{
        
        $this->connectionsByPostUUID[$postuuid] = $this->connectionsByPostUUID[$postuuid] ?? [];
        $this->connectionsByPostUUID[$postuuid][$postuuid] = $this->connectionsByPostUUID[$postuuid][$postuuid] ?? [];
        
        array_push($this->connectionsByPostUUID[$postuuid][$postuuid], $conn);
        return $conn;
    }

    /**
     * @return ConnectionInterface[][]
    */
    public function getConnectionByPostUUID(string $uuid) : array{
        return (array)$this->connectionsByPostUUID[$uuid];
    }

    public function sendByUserUUID(string $userUUID, string $data){
        try{
            $clients = $this->connectionsByUserUUID[$userUUID];
            foreach($clients as $client){
                $client->send($data);
            }
        }catch(Exception $ex){

        }
    }

    public function sendPostUUID(string $uuid, string $data){
        try{
            $clientsPostUUID = $this->connectionsByPostUUID[$uuid];
            foreach($clientsPostUUID as $k => $clientsUserUUID){
                foreach($clientsUserUUID as $client){
                    $client->send($data);
                }
            }
        }catch(Exception $ex){

        }
    }

    public function broadcastAll(string $data){
        try{
            foreach($this->connectionsByUserUUID as $k => $clients){
                foreach($clients as $client){
                    $client->send($data);
                }
            }
        }catch(Exception $ex){

        }
    }

    public function addConnectionDirectMessageByUserUUID(string $fromUserUUID, string $toUserUUID, ConnectionInterface $conn) : array{
    
        $this->connectionDirectMessageByUserUUID[$fromUserUUID] = $this->connectionDirectMessageByUserUUID[$fromUserUUID] ?? [];
        $this->connectionDirectMessageByUserUUID[$fromUserUUID][$toUserUUID] = $this->connectionDirectMessageByUserUUID[$fromUserUUID][$toUserUUID] ?? [];

        array_push($this->connectionDirectMessageByUserUUID[$fromUserUUID][$toUserUUID],$conn);

        return $this->connectionDirectMessageByUserUUID[$fromUserUUID][$toUserUUID];
    }

    public function sendDirectMessageByUserUUID(string $fromUserUUID, string $toUserUUID, string $data){
        try{

            $receivers = $this->connectionDirectMessageByUserUUID[$fromUserUUID] ?? [];
            $receiverClients = $receivers[$toUserUUID] ?? [];

            $senders = $this->connectionDirectMessageByUserUUID[$toUserUUID] ?? [];
            $senderClients = $senders[$fromUserUUID] ?? [];



            foreach($receiverClients as $client){
                $client->send($data);
            }

            foreach($senderClients as $client){
                $client->send($data);
            }
        }catch(Exception $ex){

        }
    }

    /**
     * @return ConnectionInterface[]
    */
    public function addConnectionNotifDirectMessageByUserUUID(string $uuid, ConnectionInterface $conn) : array{
        if(empty($this->connectionNotifDirectMessageByUserUUID[$uuid])){
            $this->connectionNotifDirectMessageByUserUUID[$uuid] = [$conn];
        }else{
            array_push($this->connectionNotifDirectMessageByUserUUID[$uuid], $conn);
        }
        return $this->connectionNotifDirectMessageByUserUUID[$uuid];
    }

    public function sendConnectionNotifDirectMessageByUserUUID(string $uuid, string $msg){
       try{
        $clients = $this->connectionNotifDirectMessageByUserUUID[$uuid] ?? [];

        foreach($clients as $client){
            $client->send($msg);
        }

       }catch(Exception $ex){
            dd('error send notif db', $ex);
       }
    } 
}
?>