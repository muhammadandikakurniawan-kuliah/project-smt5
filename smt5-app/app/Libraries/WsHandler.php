<?php
namespace App\Libraries;
use App\Usecases\Auth\AuthUsecase;
use App\Usecases\Chat\ChatUsecase;
use App\Usecases\Post\PostUsecase;
use Exception;
use Kint\Kint;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
class WsHandler implements MessageComponentInterface {
    
    
    /**
     * @var WsAppUserConnectionStorage
    */

    /**
     * @var AuthUsecase
    */
    private $authUsecase;

    /**
     * @var PostUsecase
    */
    private $postUsecase;

    /**
     * @var ChatUsecase
    */
    private $chatUsecase;

	protected $clientsStorage;
    public function __construct() {
        $this->clientsStorage = new WsAppUserConnectionStorage();
        $this->authUsecase = AuthUsecase::getInstance();
        $this->postUsecase = PostUsecase::getInstance();
        $this->chatUsecase = ChatUsecase::getInstance();
    }

    public function onOpen(ConnectionInterface $conn) {
        try{
            $uriQuery = $conn->httpRequest->getUri();
            parse_str(parse_url( $uriQuery, PHP_URL_QUERY), $queryParams);
            $userUUID = $queryParams['user_uuid'];
            $this->clientsStorage->addConnectionByUserUUID($userUUID, $conn);
    
            $connectionType = $queryParams['connection_type'];
            switch($connectionType){
                case WsConnectionType::POST_COMMENT:
                    $postUUID = $queryParams['post_uuid'];
                    if(!empty($postUUID)){
                        $this->clientsStorage->addConnectionByPostUUID($postUUID, $userUUID, $conn);
                    }
                    break;
                case WsConnectionType::DIRECT_MESSAGE:
                    $receiverUUID = $queryParams['receiver_uuid'];
                    $this->clientsStorage->addConnectionDirectMessageByUserUUID($userUUID, $receiverUUID, $conn);
                    break;
                case WsConnectionType::NOTIF_DM:
                    $this->clientsStorage->addConnectionNotifDirectMessageByUserUUID($userUUID, $conn);
                    break;
                default:
                    $conn->close();
            }
        }catch(Exception $ex){
            $conn->close();
        }
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        try{
            $msgData = (array)json_decode($msg);
            $userUUID = $msgData['from'];
            $commentTxt = $msgData["message"];
            $connectionType = $msgData["connection_type"];
            switch(strtoupper($connectionType)){
                case WsConnectionType::POST_COMMENT :
                    $postUUID = $msgData["post_uuid"] ?? "";
                    $response = $this->postUsecase->AddComment($postUUID, $userUUID, $commentTxt);
                    if($response->statusCode != "POST000"){
                        $from->send(json_encode($response));
                    }else{
                        $this->clientsStorage->sendPostUUID($postUUID, json_encode($response));
                    }
                    break;
                case WsConnectionType::DIRECT_MESSAGE:
                    $receiver = $msgData['receiver_uuid'];
                    $msgTxt = $msgData['message'];
                    $response = $this->chatUsecase->sendDirectMessage($userUUID, $receiver, $msgTxt);
                    if($response->statusCode != "CHAT000"){
                        $from->send(json_encode($response));
                    }else{
                        $this->clientsStorage->sendDirectMessageByUserUUID($userUUID, $receiver, json_encode($response));
                        
                        // send notif dm
                        $notifDmData = $this->chatUsecase->getListChatByUserUUID($receiver,5, 1);
                        $this->clientsStorage->sendConnectionNotifDirectMessageByUserUUID($receiver, json_encode($notifDmData));
                    }
                    break;
                case WsConnectionType::NOTIF_DM:
                    $response = $this->chatUsecase->getListChatByUserUUID($userUUID,5, 1);
                    // if($response->statusCode != "CHAT000"){
                    //     $from->send(json_encode($response));
                    // }else{
                    //     $this->clientsStorage->sendConnectionNotifDirectMessageByUserUUID($userUUID,  json_encode($response));
                    // }
                    break;
            }
        }catch(Exception $ex){
            dd($ex);
            $from->close();
        }
    }

    public function onClose(ConnectionInterface $conn) {
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
    	echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}

?>