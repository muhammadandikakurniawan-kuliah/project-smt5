<?php

namespace App\Infrastructures\Repositories;
use App\Entities\AppUserEntity;
use App\Entities\UserFollowEntity;
use App\Models\UserFollowModel;

class UserFollowRepository extends BaseRepository{

    /**
     * @var UserFollowRepository
    */
    private static $instance;

    public static function getInstance() : UserFollowRepository{

        if(self::$instance == null){
            self::$instance = new UserFollowRepository(UserFollowModel::getInstance());
        }

        return self::$instance;
    }

    function __construct(UserFollowModel $modelParam)
    {
        parent::__construct($modelParam);
    }

    public function create(UserFollowEntity $entity) : UserFollowEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        $dataInsert = $entity->toArray();
        $insertRes = $this->model->insert($dataInsert, true);
        $entity->id =$insertRes;
        return $entity;
    }

    public function update(UserFollowEntity $entity) : ?UserFollowEntity {
        $entity->updatedAt = date('Y-m-d H:i:s');
        $updateRes = $this->model->update($entity->id, $entity->toArray());
        if(!$updateRes){
            return null;
        }
        return $entity;
    }

    /**
     * @return UserFollowEntity[]
     */
    public function getFollowers(string $userId) : array  {
        $tblName = $this->model->getTable();
        $query = "
            SELECT uf.id uf_id, uf.created_at uf_created_at, usr.* FROM ".$tblName." uf
            JOIN app_users usr ON usr.uuid = uf.user_uuid
            WHERE uf.user_followed_uuid = ? AND usr.deleted_at IS NULL AND uf.deleted_at IS NULL 
        ";

        $queryResult = $this->model->query($query, $userId)->getResultArray();

        if(empty($queryResult)){
            return [];
        }

        $result = array_map(function($data){
            $resData = new UserFollowEntity();
            $resData->id = $data['uf_id'];
            $resData->createdAt = $data['uf_created_at'];
            $resData->user = new AppUserEntity($data);
            return $resData;
        }, $queryResult);

        return $result;
    }

    /**
     * @return UserFollowEntity[]
     */
    public function getFollowing(string $userId) : array  {
        $tblName = $this->model->getTable();
        $query = "
            SELECT uf.id uf_id, uf.created_at uf_created_at, usr.* FROM ".$tblName." uf
            JOIN app_users usr ON usr.uuid = uf.user_followed_uuid
            WHERE uf.user_uuid = ? AND usr.deleted_at IS NULL AND uf.deleted_at IS NULL 
        ";
        $queryResult = (array)$this->model->query($query, $userId)->getResultArray();
        $result = array_map(function($data){
            $resData = new UserFollowEntity();
            $resData->id = $data['uf_id'];
            $resData->createdAt = $data['uf_created_at'];
            $resData->user = new AppUserEntity($data);
            return $resData;
        }, $queryResult);
        return $result;
    }

    public function getByUserAndFollowewdId(string $userId, string $followedUserId, bool $deletedAt = true) : ?UserFollowEntity {
       
        $query = $this->model
        ->where("user_uuid", $userId)
        ->where("user_followed_uuid",  $followedUserId);

        if($deletedAt){
            $query = $query->where("deleted_at IS NULL");
        }

        $queryResult = $query->first();

        if(!$queryResult){
            return null;
        }

        $result = new UserFollowEntity();
        $result->id = $queryResult['id'];
        $result->userUUID = $queryResult['user_uuid'];
        $result->userFollowUUID = $queryResult['user_followed_uuid'];
        $result->createdAt = $queryResult['created_at'];
        return $result;
    }

    public function getFollowersCount(string $userId) : int  {
        $tblName = $this->model->getTable();
        $query = "
            SELECT COUNT(*) as count FROM user_follow uf
            JOIN app_users usr ON usr.uuid = uf.user_uuid
            WHERE uf.user_followed_uuid = ? AND usr.deleted_at IS NULL AND uf.deleted_at IS NULL 
        ";

        $queryResult = (array)$this->model->query($query, [$userId])->getFirstRow();

        return $queryResult['count'];
    }

    public function getFollowingCount(string $userId) : int  {
        $tblName = $this->model->getTable();
        $query = "
            SELECT COUNT(*) as count FROM user_follow uf
            JOIN app_users usr ON usr.uuid = uf.user_followed_uuid
            WHERE uf.user_uuid = ? AND usr.deleted_at IS NULL AND uf.deleted_at IS NULL 
        ";

        $queryResult = (array)$this->model->query($query, [$userId])->getFirstRow();

        return $queryResult['count'];
    }
}

?>