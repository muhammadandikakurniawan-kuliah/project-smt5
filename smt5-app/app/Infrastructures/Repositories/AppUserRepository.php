<?php

namespace App\Infrastructures\Repositories;

use App\Entities\AppUserEntity;
use App\Models\AppUserModel;
use App\Infrastructures\Repositories\RepositoryResponseModel;
use CodeIgniter\Model;
use Exception;
use CodeIgniter\Database\Exceptions\DatabaseException;
use Ramsey\Uuid\Uuid;

class AppUserRepository extends BaseRepository {

    /**
     * @var AppUserRepository
    */
    private static $instance = null;
    function __construct(AppUserModel $modelParam)
    {
        parent::__construct($modelParam);
    }

    public static function getInstance() : AppUserRepository
    {
        if (self::$instance == null) {
            self::$instance = new AppUserRepository(AppUserModel::getInstance());
        } 

        return self::$instance;
    }

    public function create(AppUserEntity $entity) : ?AppUserEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        
        $dataTable = $entity->toArray();
        $dataTable["uuid"] = Uuid::uuid4()->toString();
        $insertId = $this->model->insert($data = $dataTable, $returnID = true);

        $dataInsert = $this->model->where("id",$insertId,true)->first();
        return  new AppUserEntity($dataInsert);
    }

    public function update(AppUserEntity $entity) : ?AppUserEntity {

        $entity->updatedAt = date('Y-m-d H:i:s');
        $updateRes = $this->model->update($entity->id, $entity->toArray());
        if(!$updateRes){
            return null;
        }
        return $entity;
    } 

    public function findByEmail(string $email) : ?AppUserEntity {
        $resData = $this->model->where("email", $email)->where("deleted_at IS NULL")->first();
        if($resData){
            return  new AppUserEntity($resData);
        }
        return null;
    }

    public function findByUsername(string $username) : ?AppUserEntity {
        $resData = $this->model->where("username", $username)->where("deleted_at IS NULL")->first();
        if($resData){
            return  new AppUserEntity($resData);
        }
        return null;
    }

    /**
     * @param string[]
     * @return AppUserEntity[]
    */
    public function getByListUUID(array $listUUid) : array{
        $lenUUid = count($listUUid);
        if($lenUUid <= 0 ){
            return [];
        }
        
        $query = $this->model;

        if($lenUUid == 1){
            $query = $query->where("uuid", $listUUid[0]);
        }else{
            $query = $query ->whereIn("uuid", $listUUid);
        }
       
        $queryResult = $query->findAll();

        $result = array_map(function($data){
            return new AppUserEntity($data);
        }, $queryResult);

        return $result;
    }

    /**
     * @return AppUserEntity[]
    */
    public function search(string $search, int $limit, int $start) : array {
        $queryResult = $this->model
        ->groupStart()
            ->like("username", $search)
            ->orLike("long_name", $search)
        ->groupEnd()
        ->where("deleted_at IS NULL")
        ->orderBy("created_at","desc")
        ->findAll($limit, $start);

        $result = array_map(function($data){
            return new AppUserEntity($data);
        }, $queryResult);
        return $result;
    }

    public function getByUUID(string $uuid) : ?AppUserEntity {
        $queryResult = $this->model
        ->where("uuid", $uuid)
        ->where("deleted_at IS NULL")
        ->orderBy("created_at","desc")
        ->first();

        if(empty($queryResult)){
            return null;
        }

        $result = new AppUserEntity($queryResult);
        return $result;
    }

}

?>