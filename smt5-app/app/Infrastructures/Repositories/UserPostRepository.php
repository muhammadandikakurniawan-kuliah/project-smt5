<?php
namespace App\Infrastructures\Repositories;

use App\Entities\UserPostEntity;
use App\Models\UserPostModel;
use App\Entities\AppUserEntity;
use Ramsey\Uuid\Uuid;

class UserPostRepository extends BaseRepository{

    /**
     * @var UserPostRepository
    */
    private static $instance = null;
    function __construct(UserPostModel $modelParam)
    {
        parent::__construct($modelParam);
    }

    public static function getInstance() : UserPostRepository
    {
        if (self::$instance == null) {
            self::$instance = new UserPostRepository(UserPostModel::getInstance());
        } 

        return self::$instance;
    }

    public function create(UserPostEntity $entity) : ?UserPostEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        
        $dataTable = $entity->toArray();
        $dataTable["uuid"] = Uuid::uuid4()->toString();
        $insertId = $this->model->insert($data = $dataTable, $returnID = true);

        $dataInsert = $this->model->where("id",$insertId,true)->first();
        return  new UserPostEntity($dataInsert);
    }

    public function update(UserPostEntity $entity) : ?UserPostEntity {

        $entity->updatedAt = date('Y-m-d H:i:s');
        $updateRes = $this->model->update($entity->id, $entity->toArray());
        if(!$updateRes){
            return null;
        }
        return $entity;
    } 

    /**
     * @return UserPostEntity[]
    */
    public function search(string $search, int $limit, int $start) : array {
        $queryResult = $this->model
        ->like("text_content", $search)
        ->where("deleted_at IS NULL")
        ->orderBy("created_at","desc")
        ->findAll($limit, $start);

        $result = array_map(function($data){
            return new UserPostEntity($data);
        }, $queryResult);
        return $result;
    }


    public function getByUUID(string $uuid) : ?UserPostEntity {
        $queryResult = $this->model
        ->where("uuid", $uuid)
        ->where("deleted_at IS NULL")
        ->first();

        if(!$queryResult){
            return null;
        }

        $result = new UserPostEntity($queryResult);
        return $result;
    }

    /**
     * @return UserPostEntity[]
    */
    public function getByUserUUID(string $uuid) : array {
        $queryResult = $this->model
        ->where("user_uuid", $uuid)
        ->where("deleted_at IS NULL")
        ->orderBy("created_at","desc")
        ->findAll();

        if(!$queryResult){
            return [];
        }

        $result = array_map(function($data){
            return new UserPostEntity($data);
        }, $queryResult);
        return $result;
    }

    /**
     * @param string[]
     * @return UserPostEntity[]
    */
    public function getByListPostUUID(array $listPostUUID) : array{
        $lenUUid = count($listPostUUID);
        if($lenUUid <= 0 ){
            return [];
        }
        
        $query = $this->model;

        if($lenUUid == 1){
            $query = $query->where("uuid", $listPostUUID[0]);
        }else{
            $query = $query ->whereIn("uuid", $listPostUUID);
        }
        $query = $query->orderBy("created_at","desc");
        $queryResult = $query->findAll();

        $result = array_map(function($data){
            return new UserPostEntity($data);
        }, $queryResult);

        return $result;
    }
}

?>