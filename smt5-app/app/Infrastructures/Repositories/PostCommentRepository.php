<?php
namespace App\Infrastructures\Repositories;

use App\Entities\AppUserEntity;
use App\Entities\PostCommentEntity;
use App\Models\PostCommentModel;

class PostCommentRepository extends BaseRepository {


    /**
     * @var PostCommentRepository
    */
    private static $instance = null;

    public static function getInstance() : PostCommentRepository{

        if(self::$instance == null){
            self::$instance = new PostCommentRepository(PostCommentModel::getInstance());
        }

        return self::$instance;
    }

    public function __construct(PostCommentModel $model)
    {
        parent::__construct($model);
    }

    public function create(PostCommentEntity $entity) : PostCommentEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        $dataInsert = $entity->toArray();
        $insertRes = $this->model->insert($dataInsert, true);
        return $entity;

    }

    /**
     * @return PostCommentEntity[]
    */
    public function getByPostUUID(string $postUUID, int $limit, int $offset) : array {
        
        $commentTbl = $this->model->getTable();
        $queryStr = "SELECT cm.*, u.profile_pict_path, u.username FROM ".$commentTbl." cm 
            JOIN app_users u ON cm.user_uuid = u.uuid
            WHERE cm.post_uuid = ? 
            ORDER BY cm.created_at DESC 
            LIMIT ? OFFSET ?
        ";
        $datas = $this->model->query($queryStr, [$postUUID, $limit, $offset])->getResultArray();

        $result = [];

        foreach($datas as $data){
            $entity = new PostCommentEntity($data);
            $entity->user = new AppUserEntity([
                'uuid' => $data['user_uuid'],
                'profile_pict_path' => $data['profile_pict_path'],
                'username' => $data['username']
            ]);
            array_push($result,$entity);
        }
        return $result;

    }

}

?>