<?php

namespace App\Infrastructures\Repositories;

use CodeIgniter\Model;

abstract class BaseRepository {
    

    /**
     * @var Model
    */
    protected $model;


    function __construct(Model $modelParam)
    {
        $this->model = $modelParam;
    }

}

?>