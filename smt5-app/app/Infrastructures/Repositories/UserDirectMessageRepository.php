<?php
namespace App\Infrastructures\Repositories;

use App\Entities\AppUserEntity;
use App\Entities\UserDirectMessageEntity;
use App\Infrastructures\Repositories\BaseRepository;
use App\Models\UserDirectMessageModel;

class UserDirectMessageRepository extends BaseRepository{
    
    /**
     * @var UserDirectMessageRepository
    */
    private static $instance = null;
    function __construct(UserDirectMessageModel $modelParam)
    {
        parent::__construct($modelParam);
    }

    public static function getInstance() : UserDirectMessageRepository
    {
        if (self::$instance == null) {
            self::$instance = new UserDirectMessageRepository(UserDirectMessageModel::getInstance());
        } 

        return self::$instance;
    }


    public function create(UserDirectMessageEntity $entity) : UserDirectMessageEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        $dataInsert = $entity->toArray();
        $insertRes = $this->model->insert($dataInsert, true);
        $entity->id =$insertRes;
        return $entity;
    }

    /**
     * @return UserDirectMessageEntity[]
    */
    public function getByUserUUID(string $senderUUID, string $receiverUUID) : array {
        
        $tblName = $this->model->getTable();
        $queryStr = "SELECT *  FROM ".$tblName." 
        WHERE
            ((sender_user_uuid = ? AND receiver_user_uuid = ?) OR (sender_user_uuid = ? AND receiver_user_uuid = ?))
            AND deleted_at IS NULL  
        ORDER BY
            created_at asc
        ";
        $datas = $this->model->query($queryStr, [$senderUUID, $receiverUUID, $receiverUUID, $senderUUID])->getResultArray();

        $result = [];
        foreach($datas as $data){
            $entity = new UserDirectMessageEntity($data);
            array_push($result,$entity);
        }
        return $result;

    }

    /**
     * @return UserDirectMessageEntity[]
    */
    public function getListLatestChatByUserUUid(string $userUUUID, int $offset = 0, int $limit = 0) : array{
        $tblName = $this->model->getTable();
        $queryStr = "
        SELECT dm.*, 
            sender.username sender_username, 
            sender.profile_pict_path sender_profile_pict, 
            sender.uuid sender_uuid, 

            receiver.username receiver_username, 
            receiver.profile_pict_path receiver_profile_pict,
            receiver.uuid receiver_uuid 
        FROM (
            SELECT max(id) id, roomId FROM (
                SELECT  
                    id, 
                    (CASE WHEN um.receiver_user_uuid = ? THEN concat(?,'+',um.sender_user_uuid)
                    ELSE concat(?,'+',um.receiver_user_uuid) END) roomId
                FROM ".$tblName." um
                WHERE 
                    sender_user_uuid = ?
                    OR receiver_user_uuid = ?
            ) rm
            GROUP BY roomId
        ) rm
        JOIN ".$tblName." dm ON rm.id = dm.id
        JOIN app_users sender ON sender.uuid = dm.sender_user_uuid
        JOIN app_users receiver ON receiver.uuid = dm.receiver_user_uuid
        ORDER BY dm.id DESC
        ";

        $queryParams = [$userUUUID,$userUUUID,$userUUUID,$userUUUID,$userUUUID];

        if($limit > 0){
            $queryStr = $queryStr.'LIMIT ? OFFSET ?';
            array_push($queryParams, $limit, $offset);
        }

        $datas = $this->model->query($queryStr, $queryParams)->getResultArray();
   
        $result = [];
        foreach($datas as $data){
            $entity = new UserDirectMessageEntity($data);
            $entity->sender = new AppUserEntity([
                'username' => $data['sender_username'],
                'profile_pict_path' => $data['sender_profile_pict'],
                'uuid' =>  $data['sender_uuid'],
            ],null);
            $entity->receiver = new AppUserEntity([
                'username' => $data['receiver_username'],
                'profile_pict_path' => $data['receiver_profile_pict'],
                'uuid' =>  $data['receiver_uuid'],
            ],null);
            array_push($result,$entity);
        }
        return $result;
    }
}

?>