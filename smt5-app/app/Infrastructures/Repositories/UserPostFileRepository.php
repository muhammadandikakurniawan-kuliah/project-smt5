<?php
namespace App\Infrastructures\Repositories;

use App\Usecases\Post\Models\PostFileModel;
use Ramsey\Uuid\Uuid;
use App\Entities\UserPostFileEntity;
use App\Models\UserPostFileModel;

class UserPostFileRepository extends BaseRepository{

    /**
     * @var UserPostFileRepository
    */
    private static $instance = null;
    function __construct(UserPostFileModel $modelParam)
    {
        parent::__construct($modelParam);
    }

    public static function getInstance() : UserPostFileRepository
    {
        if (self::$instance == null) {
            self::$instance = new UserPostFileRepository(UserPostFileModel::getInstance());
        } 

        return self::$instance;
    }

    public function create(UserPostFileEntity $entity) : ?UserPostFileEntity {
        $entity->createdAt = date('Y-m-d H:i:s');
        
        $dataTable = $entity->toArray();
        $dataTable["uuid"] = Uuid::uuid4()->toString();
        $insertId = $this->model->insert($data = $dataTable, $returnID = true);

        $dataInsert = $this->model->where("id",$insertId,true)->first();
        return  new UserPostFileEntity($dataInsert);
    }

    /**
     * @param UserPostFileEntity[] $entities
     * @return UserPostFileEntity[]
    */
    public function createBulk(array $entities) : array {
        /**
         * @var UserPostFileEntity[]
        */
        $result = [];
        $now= date('Y-m-d H:i:s');
        $dataTables = [];
        foreach($entities as $entity){
            $entity->createdAt = $now;
            $dataTable = $entity->toArray();
            array_push($dataTables, $dataTable);
            array_push($result, new UserPostFileEntity($dataTable));
        }
        $this->model->insertBatch($dataTables);
        return  $result;
    }

    /**
     * @param string[]
     * @return UserPostFileEntity[]
    */
    public function getByListPostUUID(array $listPostUUID) : array{
        $lenUUid = count($listPostUUID);
        if($lenUUid <= 0 ){
            return [];
        }
        
        $query = $this->model;

        if($lenUUid == 1){
            $query = $query->where("post_uuid", $listPostUUID[0]);
        }else{
            $query = $query ->whereIn("post_uuid", $listPostUUID);
        }
       
        $queryResult = $query->findAll();

        $result = array_map(function($data){
            return new UserPostFileEntity($data);
        }, $queryResult);

        return $result;
    }
}

?>