<?php
namespace App\Infrastructures\MongoRepositories;

use \MongoDB\Client as MongoDbClient;
use \App\Database\MongoDb\Connection AS MongoConnection;
use MongoDB\Collection;
use App\Entities\PostTagEntity;

class UserPostTagRepository extends BaseRepository{
    
    /**
     * @var UserPostTagRepository
    */
    private static $instance = null;

    public static function getInstance() : UserPostTagRepository{
        if(self::$instance == null){
            self::$instance = new UserPostTagRepository(MongoConnection::getInstance());
        }
        return self::$instance;
    }

    public function __construct(MongoConnection $connection)
    {
        parent::__construct($connection, "user_post_tags");
    }

    public function create(PostTagEntity $dataEntity) : ?PostTagEntity {
        $dataEntity->createdAt = date('Y-m-d H:i:s');
        $insertRes = $this->collection->insertOne($dataEntity->toArray());
        $insertId = $insertRes->getInsertedId()->__toString();
        if(!$insertId){
            return null;
        }
        $dataEntity->id = $insertId;

        return $dataEntity;
    }

    public function updateByPostUUID(PostTagEntity $dataEntity) : ?PostTagEntity {
        $dataEntity->updatedAt = date('Y-m-d H:i:s');
        $updateRes = $this->collection->updateOne(
            ['post_uuid' => $dataEntity->postUuid], 
            ['$set' => $dataEntity->toArray()]);
        return $dataEntity;
    }

    public function getByPostUUID(string $postUUID) : ?PostTagEntity{
        $cursor = $this->collection->find(
            ["post_uuid" => $postUUID],
            ['limit' => 1]
        )->toArray();
        if(empty($cursor)){
            return null;
        }

        $data = $cursor[0]->getArrayCopy();
        
        $result = new PostTagEntity();
        $result->id = $data["_id"]->__toString();
        $result->postUuid = $data["post_uuid"];
        $result->tags = $data["tags"];
        $result->createdAt = $data["created_at"];
        $result->updatedAt = $data["updated_at"];
        $result->deletedAt = $data["deleted_at"];
        return $result;
    }

    /**
     * @return PostTagEntity[]
    */
    public function getByTag(string $tag, int $limit, int $skip) : array{
        $cursor = $this->collection->find(
            ["tags" => ['$in' => [$tag]]],
            ['limit' => $limit, 'skip' => $skip]
        )->toArray();
        if(empty($cursor)){
            return [];
        }
        
        $result = [];
        foreach($cursor as $data){
            $dataArr = $data->getArrayCopy();
            $resData = new PostTagEntity();
            $resData->id = $dataArr["_id"]->__toString();
            $resData->postUuid = $dataArr["post_uuid"];
            $resData->tags = $dataArr["tags"];
            $resData->createdAt = $dataArr["created_at"];
            $resData->updatedAt = $dataArr["updated_at"];
            $resData->deletedAt = $dataArr["deleted_at"];
            array_push($result, $resData);
        }
        
        return $result;
    }
}

?>