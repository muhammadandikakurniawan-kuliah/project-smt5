<?php
namespace App\Infrastructures\MongoRepositories;

use \MongoDB\Client as MongoDbClient;
use \App\Database\MongoDb\Connection AS MongoConnection;
use MongoDB\Collection;
use App\Entities\PostCommentEntity;

class PostCommenRepository extends BaseRepository{
    
    /**
     * @var PostCommenRepository
    */
    private static $instance = null;

    public static function getInstance() : PostCommenRepository{
        if(self::$instance == null){
            self::$instance = new PostCommenRepository(MongoConnection::getInstance());
        }
        return self::$instance;
    }

    public function __construct(MongoConnection $connection)
    {
        parent::__construct($connection, "post_comments");
    }

    /**
     * @param PostCommentEntity
     * @return PostCommentEntity
    */
    public function create(PostCommentEntity $dataEntity) : PostCommentEntity {
        $now = date('Y-m-d H:i:s');
        $dataEntity->createdAt = $now;
        $insertRes = $this->collection->insertOne($dataEntity->toArray());
        // MongoDB\BSON\ObjectId
        $insertedId = $insertRes->getInsertedId();
        $dataEntity->id = $insertedId->__toString();
        return $dataEntity;
    }

    /**
     * @return PostCommentEntity[]
    */
    public function getByPostUUID(string $postUUID) : array{
        $cursor = $this->collection->find(
            ["post_uuid" => $postUUID],
            ["sort" => ["created_at" => -1]]
        )->toArray();
        if(empty($cursor)){
            return [];
        }

        $data = $cursor[0]->getArrayCopy();
        
        $result = [];
        foreach($cursor as $dataTable){
            $dataEntity = new PostCommentEntity();
            $dataEntity->id = $data["_id"]->__toString();
            $dataEntity->userUUID = $dataTable['user_uuid'];
            $dataEntity->postUUID = $dataTable['post_uuid'];
            $dataEntity->commentTxt = $dataTable['comment_txt'];
            $dataEntity->createdAt = $dataTable['created_at'];
            $dataEntity->updatedAt = $dataTable['updated_at'] ?? null;
            $dataEntity->deletedAt = $dataTable['deleted_at'] ?? null;
            array_push($result,$dataEntity);
        }
        
        return $result;
    }
}

?>