<?php
namespace App\Infrastructures\MongoRepositories;
use \MongoDB\Client as MongoDbClient;
use \App\Database\MongoDb\Connection AS MongoConnection;
use MongoDB\Collection;

abstract class BaseRepository {

    /**
     * @var MongoDbClient
    */
    protected $client;

    /**
     * @var Collection
    */
    protected $collection;

    function __construct(MongoConnection $connection, string $collectionName)
    {
        $this->client = $connection->getClient();
        
        $dbName = getenv("MONGODB_DBNAME");
        $this->collection = $this->client->selectDatabase($dbName)->selectCollection($collectionName);
    }

}

?>