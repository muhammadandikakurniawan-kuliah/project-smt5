<?php
namespace App\Infrastructures\MongoRepositories;

use App\Entities\TrendingTagEntity;
use \MongoDB\Client as MongoDbClient;
use \App\Database\MongoDb\Connection AS MongoConnection;
use App\Entities\ContentTagEntity;
use MongoDB\Collection;
use App\Entities\PostTagEntity;

class ContentTagRepository extends BaseRepository{
    
    /**
     * @var ContentTagRepository
    */
    private static $instance = null;

    public static function getInstance() : ContentTagRepository{
        if(self::$instance == null){
            self::$instance = new ContentTagRepository(MongoConnection::getInstance());
        }
        return self::$instance;
    }

    public function __construct(MongoConnection $connection)
    {
        parent::__construct($connection, "content_tags");
    }

    /**
     * @param ContentTagEntity[]
     * @return ContentTagEntity[]
    */
    public function createMany(array $dataEntities) : array {
        $now = date('Y-m-d H:i:s');
        $dataInsert = [];
        foreach($dataEntities as $dataEntity){
            $dataEntity->createdAt = $now;
            array_push($dataInsert, $dataEntity->toArray());
        }
        $insertRes = $this->collection->insertMany($dataInsert);
        return $dataEntities;
    }



    /**
     * @return ContentTagEntity[]
    */
    public function search(string $tag, int $limit, int $skip) : array{
        
        $cursor = $this->collection->distinct(
            "tag",
            ["tag" => ['$regex' => '^.*'.$tag.'.*$', '$options'=>'i']],
            ['limit' => $limit, 'skip' => $skip]
        );

        if(empty($cursor)){
            return [];
        }

        $result = [];
        foreach($cursor as $data){
            $resData = new ContentTagEntity();
            $resData->tag = $data;
            array_push($result, $resData);
        }
        
        
        return $result;
    }


    /**
     * @return ContentTagEntity[]
    */
    public function searchOld(string $tag, int $limit, int $skip) : array{
        
        $cursor = $this->collection->find(
            ["tag" => ['$regex' => '^.*'.$tag.'.*$', '$options'=>'i']],
            ['limit' => $limit, 'skip' => $skip]
        )->toArray();
        if(empty($cursor)){
            return [];
        }

        $result = [];
        foreach($cursor as $data){
            $dataArr = $data->getArrayCopy();
            $resData = new ContentTagEntity();
            $resData->tag = $dataArr["tag"];
            $resData->id = $dataArr["_id"]->__toString();
            $resData->createdAt = $dataArr["created_at"];
            $resData->updatedAt = $dataArr["updated_at"];
            $resData->deletedAt = $dataArr["deleted_at"];
            array_push($result, $resData);
        }
        
        
        return $result;
    }

    /**
     * @return TrendingTagEntity[]
    */
    public function getTrandingTags(int $limit) : array {
        $pipeline = [
            ['$sortByCount' => '$tag'],
            ['$limit' => $limit]
        ];
        $cursor = $this->collection->aggregate($pipeline);
       
        if(empty($cursor)){
            return [];
        }

        $result = [];
        foreach($cursor as $data){
            $resData = new TrendingTagEntity();
            $resData->tag = $data["_id"];
            $resData->count = $data["count"];
            array_push($result, $resData);
        }
        
        return $result;
    }
}

?>