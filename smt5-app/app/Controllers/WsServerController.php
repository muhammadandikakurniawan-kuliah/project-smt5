<?php

namespace App\Controllers;
use App\Models\AppUserModel;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\RepositoryResponseModel;
use App\Entities\AppUserEntity;
use App\Usecases\Auth\AuthUsecase;
use App\Usecases\Auth\Models\RegisterRequestModel;
use App\Usecases\Post\Models\GetPostRequestModel;
use App\Usecases\Post\Models\GetPostResponseModel;
use App\Usecases\Post\PostUsecase;
use DateTime;
use App\Database\MongoDb\Connection as MongoDbConnection;
use App\Libraries\WsHandler;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;


class WsServerController extends BaseController {

    public function index(){
        if(!is_cli()){
            die("Not found");
        }
        $server = IoServer::factory(
			new HttpServer(
				new WsServer(
					new WsHandler()
				)
			),
			getenv('WEB_SOCKET_SERVER_PORT')
		);
        $server->run();
    }

}


?>