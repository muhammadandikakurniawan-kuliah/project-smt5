<?php

namespace App\Controllers;
use App\Models\AppUserModel;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\RepositoryResponseModel;
use App\Entities\AppUserEntity;
use App\Helpers\EmailHelper;
use App\Usecases\Auth\AuthUsecase;
use App\Usecases\Auth\Models\RegisterRequestModel;
use App\Usecases\Post\PostUsecase;
use CodeIgniter\Exceptions\PageNotFoundException;
use DateTime;
use PharIo\Manifest\Library;
use CodeIgniter\Email\Email;
use CodeIgniter\Files\File;
use Config\Email as ConfigEmail;
use Aws\S3\S3Client;
use Exception;
use App\Helpers\S3Helper;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Dto\AppUserDto;
use App\Usecases\Auth\Models\GetUserRequestModel;
use CodeIgniter\Session\Session;

class AuthController extends BaseController
{

    /**
     * @var AuthUsecase
    */
    private $authUsecase;

    /**
     * @var PostUsecase
    */
    private $postUsecase;

    function __construct()
    {
        $this->authUsecase = AuthUsecase::getInstance();
        $this->postUsecase = PostUsecase::getInstance();
    }

    public function registerView()
    {
        $viewData = [];
        $alerts = [];
        try{
            $registerRequestModel = new RegisterRequestModel();
            $viewData["requestData"] = $registerRequestModel->toArray();
        }catch(Exception $ex){
            $viewData["errors"] = [
                "Internal server error"
            ];
            dd($ex);
        }
        $viewData["alerts"] = $alerts;
        return view('auth/register', $viewData);
    }

    public function registerProcess()
    {
        $viewData = [];
        $alerts = [];
        try{

            $requestData = $this->request->getPost();

            $registerRequestModel = new RegisterRequestModel();
            $registerRequestModel->coverPict = $requestData["coverPict"];
            $registerRequestModel->profilePict = $requestData["profilePict"];
            $registerRequestModel->firstName = $requestData["firstName"];
            $registerRequestModel->lastName = $requestData["lastName"];
            $registerRequestModel->email = $requestData["email"];
            $registerRequestModel->username = $requestData["username"];
            $registerRequestModel->password = $requestData["password"];
            $registerRequestModel->confirmationPasword = $requestData["confirmationPasword"];

            $result = $this->authUsecase->register($registerRequestModel);
            if($result->statusCode == "REG000"){
                return view('auth/register_success', ["userData" => $result->data]);
            }
            if($result->errorMessage != null){
                array_push($alerts, ["type" => "alert-danger", "message" => $result->errorMessage]);
            }
            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }
            $viewData["registerResponse"] = $result;
            $viewData["requestData"] = $requestData;

        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            dd($ex);
        }

        $viewData["alerts"] = $alerts;

        return view('auth/register',$viewData);
    }

    public function activationAccountProcess(string $token){
        $viewData = [];
        $alerts = [];
        try{
            $result = $this->authUsecase->activateAccount($token);
            if($result->statusCode == "ACT000"){
                $viewData["isSuccess"] = true;
                array_push($alerts, ["type" => "alert-success", "message" => "Activation account successfull"]);
            }
            if($result->errorMessage != null){
                array_push($alerts, ["type" => "alert-danger", "message" => $result->errorMessage]);
            }
            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }
            $viewData["userData"] = $result->data;
        }catch(Exception $ex){
            $viewData["errors"] = [
                "Internal server error"
            ];
            dd($ex);
        }
        $viewData["alerts"] = $alerts;
        return view('auth/activation_account', $viewData);
    }

    public function loginView(){
        $viewData = [];
        $alerts = [];
        try{

        }catch(Exception $ex){
            $viewData["errors"] = [
                "Internal server error"
            ];
            dd($ex);
        }
        $viewData["alerts"] = $alerts;
        return view('auth/login', $viewData);
    }
    
    public function loginProcess(){
        $viewData = [];
        $alerts = [];
        try{

            $requestData = $this->request->getPost();

            $email = $requestData["email"];
            $pass = $requestData["password"];

            $result = $this->authUsecase->login($email, $pass);
            if($result->statusCode == "LOGIN000"){
                session()->start();

                session()->set(["USER_DATA" => $result->data]);

                return redirect()->to(base_url('/'));
            }
            if($result->errorMessage != null){
                array_push($alerts, ["type" => "alert-danger", "message" => $result->errorMessage]);
            }
            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }
            $viewData["registerResponse"] = $result;
            $viewData["requestData"] = $requestData;

        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            dd($ex);
        }

        $viewData["alerts"] = $alerts;

        return view('auth/login',$viewData);
    }

    function searchUser(){
        $requestData = (array)$this->request->getJSON();
        // var_dump($requestData);
        $usecaseReq = new GetUserRequestModel();
        $usecaseReq->search = $requestData["searchKey"];
        $usecaseReq->perpage = $requestData["perPage"];
        
        $result = $this->authUsecase->searchUser($usecaseReq);
        // dd($usecaseReq);
        echo json_encode($result);
    }

    public function logout(){
        session()->destroy("USER_DATA");
        return redirect()->to(base_url('/auth/login'));
    }

    public function userDetail(string $uuid){
        
        $viewData = [];
        $userLoginData = $this->getUserData();
        $viewData["userData"] = $userLoginData;
        $alerts = [];
        try{

            $getDetailAccountRes = $this->authUsecase->getUserByUUID($uuid, $userLoginData["uuid"]);
            if($getDetailAccountRes->statusCode == "AUTH500"){
                array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            }
            if($getDetailAccountRes->statusCode == "AUTH204"){
                throw PageNotFoundException::forPageNotFound();
            }

            $accountData = $getDetailAccountRes->data[0];

            $getPostRes = $this->postUsecase->getListPostByUser($accountData->uuid);
            if($getPostRes->statusCode != "POST000"){
                throw new Exception("internal server error");
            }

            $viewData["accountData"] = $accountData;
            $viewData["listPost"] = $getPostRes->data ?? [];
        }catch(Exception $ex){
            if($ex instanceof PageNotFoundException){
                throw PageNotFoundException::forPageNotFound();
            }
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            dd($ex);
        }

        $viewData["alerts"] = $alerts;
        return view('auth/user_detail',$viewData);
    }

    function followUser(){
        $requestData = (array)$this->request->getJSON();
        // var_dump($requestData);
        $userId = $requestData["userId"];
        $followedUserId = $requestData["followedUserId"];
        $follow = $requestData["follow"];
        
        $result = $this->authUsecase->followUser($userId, $followedUserId, $follow);
        // dd($usecaseReq);
        echo json_encode($result);
    }

    function getFollowUserData(){
        $requestData = (array)$this->request->getJSON();
        $userId = $requestData["userId"];
        $isFollowers = (bool)$requestData["getFollowersData"] ?? false;
        
        $result = $this->authUsecase->getfollowUserData($userId, $isFollowers);
        // dd($usecaseReq);
        echo json_encode($result);
    }
}
