<?php

namespace App\Controllers;
use App\Models\AppUserModel;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\RepositoryResponseModel;
use App\Entities\AppUserEntity;
use App\Usecases\Auth\AuthUsecase;
use App\Usecases\Auth\Models\RegisterRequestModel;
use App\Usecases\Post\Models\GetPostRequestModel;
use App\Usecases\Post\Models\GetPostResponseModel;
use App\Usecases\Post\PostUsecase;
use DateTime;
use App\Database\MongoDb\Connection as MongoDbConnection;

class Home extends BaseController
{

    /**
     * @var AuthUsecase
    */
    private $authUsecase;

    /**
     * @var PostUsecase
    */
    private $postUsecase;

    function __construct()
    {
        $this->authUsecase = AuthUsecase::getInstance();
        $this->postUsecase = PostUsecase::getInstance();
    }

   

    public function index()
    {
        $tag = $this->request->getVar("tag");
        
        $viewData = [];
        $alerts = [];
        
        $viewData["userData"] = $this->getUserData();
        $viewData["alerts"] = $alerts;
        $viewData["trendingData"] = $this->postUsecase->getTrandingTag()->data;
        $viewData["tag"] = $tag ?? "";
        
        $getPostRequest = new GetPostRequestModel();
        $getPostRequest->perpage = 10;
        $result = new GetPostResponseModel();
        if(!empty($tag)){
            $getPostRequest->tag = $tag;
            $result = $this->postUsecase->getPostByTag($getPostRequest);
        }else{
            $result = $this->postUsecase->getPost($getPostRequest); 
        }
        
        $viewData["listPost"] = $result->data ?? [];
        return view('home/index', $viewData);
    }
}
