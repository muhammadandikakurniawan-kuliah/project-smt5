<?php

namespace App\Controllers;
use App\Database\MongoDb\Connection;
use App\Infrastructures\MongoRepositories\UserPostTagRepository;
use App\Models\AppUserModel;
use App\Infrastructures\Repositories\AppUserRepository;
use App\Infrastructures\Repositories\RepositoryResponseModel;
use App\Entities\AppUserEntity;
use App\Helpers\EmailHelper;
use App\Usecases\Auth\AuthUsecase;
use App\Usecases\Auth\Models\RegisterRequestModel;
use DateTime;
use PharIo\Manifest\Library;
use CodeIgniter\Email\Email;
use CodeIgniter\Files\File;
use Config\Email as ConfigEmail;
use Aws\S3\S3Client;
use Exception;
use App\Helpers\S3Helper;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Dto\AppUserDto;
use App\Entities\PostTagEntity;
use CodeIgniter\Session\Session;

class TestViewController extends BaseController
{
    public function index(){

        $repo = UserPostTagRepository::getInstance();

        $testContent = "sdsdfdsfsdf #tg1 sdfsdf #tg2 #tg3 #tg4 asdasda #tg5";
        preg_match_all('/#\S+/', $testContent, $matches);
        $tags = $matches[0];

        // $dataEntity = new PostTagEntity();
        // $dataEntity->postUuid = "asdasd-123123-asdad-123-1";
        // $dataEntity->tags = $tags;
        // $insetRes = $repo->create($dataEntity);



        $dataEntity = $repo->getByPostUUID("asdasd-123123-asdad-123-1");
        dd($dataEntity instanceof PostTagEntity);
        $dataEntity->tags = ["#tag_update1-123","#tag_update2","#tag_update3","#tag_update4"];
        $updateRes = $repo->updateByPostUUID($dataEntity);
        dd($updateRes);
    }
}
