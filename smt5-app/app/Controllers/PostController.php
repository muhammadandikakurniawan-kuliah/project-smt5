<?php
namespace App\Controllers;
use App\Usecases\Post\Models\GetPostRequestModel;
use CodeIgniter\Exceptions\PageNotFoundException;
use CodeIgniter\HTTP\Request;
use App\Usecases\Post\PostUsecase;
use App\Usecases\Post\Models\CreatePostRequestModel;
use App\Usecases\Post\Models\PostFileModel;
use Exception;

class PostController extends BaseController{

    /**
     * @var PostUsecase
    */
    private $postUsecase;

    public function __construct()
    {
        $this->postUsecase = PostUsecase::getInstance();
    }

    public function createView(){
        $viewData = [];
        $alerts = [];
        
        
        $viewData["userData"] = $this->getUserData();
        $viewData["alerts"] = $alerts;

        return view('post/create', $viewData);
    }
    
    function createProcess(){

        $viewData = [];
        $alerts = [];
        try{
            $viewData["userData"] = $this->getUserData();
            
            $requestData = $this->request->getPost();
            $userData = (array)session()->get("USER_DATA");
            $usecaseReq = new CreatePostRequestModel();
            $usecaseReq->textContent = $requestData["textContent"];
            $usecaseReq->userUUID = $userData["uuid"];
            $usecaseReq->files = $this->setupUsecaseRequestFile();
            $result = $this->postUsecase->createPost($usecaseReq);
    
            if($result->statusCode == "POST000"){
                array_push($alerts, ["type" => "alert-success", "message" => "post created successfull"]);
                $viewData["alerts"] = $alerts;
                return view('post/create', $viewData);
            }
            if($result->errorMessage != null){
                array_push($alerts, ["type" => "alert-danger", "message" => $result->errorMessage]);
            }
            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }

        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            dd($ex);
        }

        $viewData["alerts"] = $alerts;
        return view('post/create',$viewData);

    }

    /**
     * @return PostFileModel[]
    */
    private function setupUsecaseRequestFile() : array {
        $result = [];
        $postFiles = $this->request->getFileMultiple("postFiles") ?? [];

        foreach($postFiles as $file){
            $fileReqData = new PostFileModel();
            $fileReqData->fileName = $file->getName();
            $fileReqData->fileType = $file->getMimeType();

            if(!$fileReqData->fileType){
                continue;
            }

            $path = $file->getRealPath();
            $imagedata = file_get_contents($path);
            $base64 = base64_encode($imagedata);
            $fileReqData->fileBase64 = $base64;

            array_push($result,$fileReqData);
        }

        return $result;
    }

    function detailView(string $postUUID){
        $viewData = [];
        $alerts = [];
        try{
            $viewData["userData"] = $this->getUserData();

            $result = $this->postUsecase->getPostDetail($postUUID, 1, 5);
            if(empty($result->data) || $result->statusCode == "POST404" || $result->statusCode == "POSTUSR404"){
                throw PageNotFoundException::forPageNotFound();
            }
            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }

            $viewData["postData"] = $result->data[0];

        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            if($ex instanceof PageNotFoundException){
                throw PageNotFoundException::forPageNotFound();
            }
            dd($ex);  
        }

        $viewData["alerts"] = $alerts;
        // dd($viewData);
        return view('post/detail',$viewData);
    }

    public function getCommentPagination(){
        $requestData = (array)$this->request->getJSON();
        $postUUID = $requestData["post_uuid"];
        $commentPage = (int)$requestData["comment_page"] ?? 1;
        $commentLen = (int)$requestData["comment_length"] ?? 10;
        $response = $this->postUsecase->getPostDetail($postUUID,$commentPage,$commentLen);
        echo json_encode($response);
    }

    function editView(string $postUUID){
        $viewData = [];
        $alerts = [];
        try{
            $userData = $this->getUserData();
            $viewData["userData"] = $userData;

            $result = $this->postUsecase->getPostDetail($postUUID);
            if(empty($result->data) || $result->statusCode == "POST404" || $result->statusCode == "POSTUSR404"){
                throw PageNotFoundException::forPageNotFound();
            }

            $postData = $result->data[0];
            if($userData["uuid"] != $postData->user->uuid){
                throw PageNotFoundException::forPageNotFound();
            }

            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }

            $viewData["postData"] = $postData;

        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            if($ex instanceof PageNotFoundException){
                throw PageNotFoundException::forPageNotFound();
            }
            dd($ex);  
        }

        $viewData["alerts"] = $alerts;
        // dd($viewData);
        return view('post/edit',$viewData);
    }

    function editProcess(string $postUUID){
        $viewData = [];
        $alerts = [];
        try{

            $requestData = $this->request->getPost();
            $userData = $this->getUserData();
            $viewData["userData"] = $userData;

            $updateReq = new CreatePostRequestModel();
            $updateReq->userUUID = $userData["uuid"];
            $updateReq->postUUID = $postUUID;
            $updateReq->textContent = $requestData["textContent"];
            $result = $this->postUsecase->updatePost($updateReq);
            if(empty($result->data) || $result->statusCode == "POST404" || $result->statusCode == "POSTUSR404"){
                throw PageNotFoundException::forPageNotFound();
            }
            if($result->statusCode != "POST000"){
                throw new Exception($result->errorMessage);
            }else{
                array_push($alerts, ["type" => "alert-success", "message" => "post created successfull"]);
            }

            if(count($result->validationErrors)){
                foreach($result->validationErrors as $err){
                    array_push($alerts, ["type" => "alert-warning", "message" => $err]);
                }
            }

            $viewData["postData"] = $result->data;


        }catch(Exception $ex){
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            if($ex instanceof PageNotFoundException){
                throw PageNotFoundException::forPageNotFound();
            }
            dd($ex);  
        }

        $viewData["alerts"] = $alerts;
        // dd($viewData);
        return view('post/edit',$viewData);
    }

    function deleteProcess(){
        $userData = (array)session()->get("USER_DATA");
        $requestData = (array)$this->request->getJSON();
        $postUUID = $requestData["post_uuid"];
        $response = $this->postUsecase->delete($postUUID,$userData["uuid"]);
        echo json_encode($response);
    }

    function searchPost(){
        $requestData = (array)$this->request->getJSON();

        $usecaseReq = new GetPostRequestModel();
        $usecaseReq->tag = $requestData["tag"];
        $usecaseReq->search = $requestData["searchKey"];
        $usecaseReq->perpage = $requestData["perPage"];
        $usecaseReq->page = $requestData["page"];

        $result = $this->postUsecase->getPost($usecaseReq);

        echo json_encode($result);
    }

    function searchTag(){
        $requestData = (array)$this->request->getJSON();

        $usecaseReq = new GetPostRequestModel();
        $usecaseReq->tag = $requestData["searchKey"];
        $usecaseReq->perpage = $requestData["perPage"];

        $result = $this->postUsecase->searchTag($usecaseReq);

        echo json_encode($result);
    }

    function getPostByTag(){
        $requestData = (array)$this->request->getJSON();

        $usecaseReq = new GetPostRequestModel();
        $usecaseReq->tag = $requestData["searchKey"];
        $usecaseReq->perpage = $requestData["perPage"];

        $result = $this->postUsecase->getPostByTag($usecaseReq);

        echo json_encode($result);
    }

    function createProcessJson(){
        $requestData = (array)$this->request->getJSON();

        
        $caption = $requestData["caption"];
        $files = (array)$requestData["files"];
        
        $userData = (array)session()->get("USER_DATA");
        $usecaseReq = new CreatePostRequestModel();
        $usecaseReq->textContent = $caption;
        $usecaseReq->userUUID = $userData["uuid"];
        $usecaseReq->files = [];
        foreach($files as $file){
            $file = (array)$file;
            $fileData = new PostFileModel();
            $fileData->fileName = $file["name"] ?? "";
            $fileData->fileType = $file["type"] ?? "";
            $fileData->fileBase64 = $file["base64"] ?? "";
            array_push($usecaseReq->files, $fileData);
        }

        $resp = $this->postUsecase->createPost($usecaseReq);

        echo json_encode($resp);
    }
}

?>