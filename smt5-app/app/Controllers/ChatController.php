<?php
namespace App\Controllers;
use App\Controllers\BaseController;
use App\Dto\AppUserDto;
use App\Usecases\Chat\ChatUsecase;
use CodeIgniter\Exceptions\PageNotFoundException;
use Exception;
use App\Usecases\Auth\AuthUsecase;

class ChatController extends BaseController{

    /**
     * @var AuthUsecase
    */
    private $authUsecase;

    /**
     * @var ChatUsecase
    */
    private $chatUsecase;

    public function __construct()
    {
        $this->authUsecase = AuthUsecase::getInstance();
        $this->chatUsecase = ChatUsecase::getInstance();
    }

    public function directMessageView($userUUID = ""){
        $viewData = [
            'messages' => [],
            'alerts' => [],
            'accountData' => new AppUserDto(null, null),
            'listDm' => []
        ];
        $alerts = [];
        try{


            $userData = $this->getUserData();
            $viewData["userData"] = $userData;

            $getListDm = $this->chatUsecase->getListChatByUserUUID($userData['uuid'], 0, 0);
            if($getListDm->statusCode != "CHAT000"){
                throw new Exception($getListDm->errorMessage);
            }
            $viewData["listDm"] = $getListDm->data;

            if($userUUID){
                if($userUUID == $userData['uuid']){
                    throw PageNotFoundException::forPageNotFound();
                }
    
                $getDetailAccountRes = $this->authUsecase->getUserByUUID($userUUID);
                if($getDetailAccountRes->statusCode == "AUTH500"){
                    array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
                }
                if($getDetailAccountRes->statusCode == "AUTH204"){
                    throw PageNotFoundException::forPageNotFound();
                }
                $accountData = $getDetailAccountRes->data[0];
                $viewData["accountData"] = $accountData;
    
                $viewModelData = $this->chatUsecase->getDirectMessageData($userData['uuid'], $accountData->uuid);
                if($viewModelData->statusCode != "CHAT000"){
                    array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
                }
                if($viewModelData->statusCode == "AUTH204"){
                    throw PageNotFoundException::forPageNotFound();
                }
    
                $viewData["messages"] = $viewModelData->data->messages;
            }
        }catch(Exception $ex){
            if($ex instanceof PageNotFoundException){
                throw PageNotFoundException::forPageNotFound();
            }
            array_push($alerts, ["type" => "alert-danger", "message" => "internal server error"]);
            dd($ex);
        }

        $viewData["alerts"] = $alerts;
        return view('chat/direct_message', $viewData);
    }

    function getListLatestDmByUserUUID(){
        $requestData = (array)$this->request->getJSON();

        $perpage = $requestData["perPage"] ?? 1;
        $page = $requestData["page"] ?? 1;

        $userData = $this->getUserData();

        $resp = $this->chatUsecase->getListChatByUserUUID($userData['uuid'], $perpage, $page);

        echo json_encode($resp);
    }

}

?>