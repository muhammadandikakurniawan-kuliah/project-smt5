<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index', ['filter' => 'authfilter']);

$routes->group("/", ['filter' => 'authfilter'], static function($routes){
    $routes->get("","Home::index'");
    $routes->get("logout","AuthController::logout");
    $routes->group("post",static function($postRoutes){
        $postRoutes->get("create","PostController::createView");
        $postRoutes->post("create","PostController::createProcess");
        $postRoutes->delete("/","PostController::deleteProcess");
        $postRoutes->get("edit/(:any)","PostController::editView/$1");
        $postRoutes->post("edit/(:any)","PostController::editProcess/$1");
        $postRoutes->get("(:any)","PostController::detailView/$1");
        $postRoutes->post("search","PostController::searchPost");
        $postRoutes->post("search-tag","PostController::searchTag");
        $postRoutes->post("get-by-tag","PostController::getPostByTag");
        $postRoutes->post("get-comments","PostController::getCommentPagination");
    });

    $routes->group("user",static function($postRoutes){
        $postRoutes->post("search","AuthController::searchUser");
        $postRoutes->post("follow-user","AuthController::followUser");
        $postRoutes->post("get-follow-user","AuthController::getFollowUserData");
    });

    $routes->group("chat",static function($chatRoutes){
        $chatRoutes->get("direct_message/(:any)","ChatController::directMessageView/$1");
        $chatRoutes->get("direct_message","ChatController::directMessageView");
        $chatRoutes->post("latest-dm","ChatController::getListLatestDmByUserUUID");
    });

    $routes->get('user-detail/(:any)', 'AuthController::userDetail/$1');
});

$routes->group("auth", ['filter' => 'noLoginFilter'], static function($routes){
    $routes->get("register","AuthController::registerView");
    $routes->post("register","AuthController::registerProcess");
    $routes->get("activate-account/(:any)","AuthController::activationAccountProcess/$1");
    $routes->get("login","AuthController::loginView");
    $routes->post("login","AuthController::loginProcess");
});

$routes->group("api", static function($apiRoutes){
    $apiRoutes->group("post", static function($authRoutes){
        $authRoutes->post("create","PostRestController::create");
    });
});


$routes->get("test", "TestViewController::index");

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
