<?php

namespace App\Dto;

class BaseResponseModel {


    /**
     * @var string 
    */
    public $statusCode = "";

    /**
     * @var string 
    */
    public $message = "";

    /**
     * @var mixed 
    */
    public $errorMessage = null;

    /**
     * @var object 
    */
    public $data = null;

}

?>