<?php
namespace App\Dto;

use App\Entities\UserPostFileEntity;

class UserPostFileDto {

    /**
     * @var string 
    */
    public $postUuid = "";

    /**
     * @var string
    */
    public $filePath;

    /**
     * @var string
    */
    public $fileName;

    /**
     * @var string
    */
    public $fileType;
    
    /**
     * @var bool
    */
    public $isImage;

    /**
     * @var bool
    */
    public $isVideo;

    function __construct(
        array $dataTable = null,
        UserPostFileEntity $dataEntity = null
    ){
        if($dataTable != null && count($dataTable) > 0){
            $this->postUuid = $dataTable["post_uuid"];
            $this->fileName = $dataTable["file_name"];
            $this->filePath = $dataTable["file_path"];
            $this->fileType = $dataTable["file_type"];
            return;
        }
        if($dataEntity != null){
            $this->postUuid = $dataEntity->postUuid;
            $this->filePath = $dataEntity->filePath;
            $this->fileName = $dataEntity->fileName;
            $this->fileType = $dataEntity->fileType;
            return;
        }

    }
}

?>