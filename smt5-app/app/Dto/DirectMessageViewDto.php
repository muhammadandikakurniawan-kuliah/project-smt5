<?php

namespace App\Dto;

use App\Entities\AppUserEntity;

class DirectMessageViewDto {

    /**
     * @var int 
    */
    public $id;

    /**
     * @var AppUserDto 
    */
    public $user;

    /**
     * @var AppUserDto 
    */
    public $targetUser;

    /**
     * @var DirectMessageDto[] 
    */
    public $messages = [];

    function __construct(){

    }

}

?>