<?php

namespace App\Dto;

use App\Entities\AppUserEntity;

class DirectMessageDto {

    /**
     * @var int 
    */
    public $id;

    /**
     * @var AppUserDto 
    */
    public $sender;

    /**
     * @var AppUserDto 
    */
    public $receiver;

    /**
     * @var string 
    */
    public $message;

    /**
     * @var \DateTime
    */
    public $createdAt;

    function __construct(){

    }

}

?>