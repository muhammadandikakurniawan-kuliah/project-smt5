<?php

namespace App\Dto;

use App\Entities\AppUserEntity;

class AppUserDto {

    /**
     * @var string 
    */
    public $uuid = "";

    /**
     * @var string 
    */
    public $longName = "";

    /**
     * @var string 
    */
    public $username = "";

    /**
     * @var string 
    */
    public $email = "";

    /**
     * @var string 
    */
    public $phoneNumber = "";

    /**
     * @var \DateTime
    */
    public $activatedAt;

    /**
     * @var string
    */
    public $profilePictPath;

    /**
     * @var string
    */
    public $coverPictPath;

    /**
     * @var bool
     */
    public $isFollowed = false;

    /**
     * @var int
     */
    public $followersCount = 0;

    /**
     * @var int
     */
    public $followingCount = 0;

    function __construct(
        array $dataTable = null,
        AppUserEntity $dataEntity = null
    ){
        if($dataTable != null && count($dataTable) > 0){
            $this->uuid = $dataTable["uuid"];
            $this->longName = $dataTable["long_name"];
            $this->username = $dataTable["username"];
            $this->email = $dataTable["email"];
            $this->phoneNumber = $dataTable["phone_number"];
            $this->activatedAt = $dataTable["activated_at"];
            $this->profilePictPath = $dataTable["profile_pict_path"];
            $this->coverPictPath = $dataTable["cover_pict_path"];
            return;
        }
        if($dataEntity != null){
            $this->uuid = $dataEntity->uuid;
            $this->longName = $dataEntity->longName;
            $this->username = $dataEntity->username;
            $this->email = $dataEntity->email;
            $this->phoneNumber = $dataEntity->phoneNumber;
            $this->activatedAt = $dataEntity->activatedAt;
            $this->profilePictPath = $dataEntity->profilePictPath;
            $this->coverPictPath = $dataEntity->coverPictPath;
            return;
        }

    }

}

?>