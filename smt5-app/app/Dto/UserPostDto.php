<?php
namespace App\Dto;
use App\Entities\UserPostEntity;
use DateTime;

class UserPostDto {

    /**
     * @var string 
    */
    public $uuid = "";

    /**
     * @var string 
    */
    public $userUuid = "";

    /**
     * @var AppUserDto 
    */
    public $user;

    /**
     * @var string
    */
    public $textContent;

    /**
     * @var DateTime
     */
    public $createdAt;
    /**
     * @var DateTime
     */
    public $updatedAt;
    /**
     * @var DateTime
     */
    public $deletedAt;

    /**
     * @var UserPostFileDto[]
    */
    public $files = [];

    /**
     * @var PostCommentDto[]
    */
    public $comments = [];

    function __construct(
        array $dataTable = null,
        UserPostEntity $dataEntity = null
    ){
        if($dataTable != null && count($dataTable) > 0){
            $this->uuid = $dataTable["uuid"];
            $this->textContent = $dataTable["text_content"];
            return;
        }
        if($dataEntity != null){
            $this->uuid = $dataEntity->uuid;
            $this->userUuid = $dataEntity->userUuid;
            $this->textContent = $dataEntity->textContent;
            $this->createdAt = $dataEntity->createdAt;
            $this->updatedAt = $dataEntity->updatedAt;
            $this->deletedAt = $dataEntity->deletedAt;
            $this->files = [];
            return;
        }

    }
}

?>