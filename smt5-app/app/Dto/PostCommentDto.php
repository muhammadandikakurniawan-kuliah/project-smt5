<?php
namespace App\Dto;

use App\Entities\PostCommentEntity;
use App\Entities\UserPostFileEntity;

class PostCommentDto {

    /**
     * @var string 
    */
    public $postUuid = "";

    /**
     * @var string 
    */
    public $userUuid = "";

    /**
     * @var string
    */
    public $commentTxt;

    /**
     * @var \DateTime
    */
    public $createdAt;

    /** 
     * @var AppUserDto 
    */
    public $user;


    function __construct(PostCommentEntity $dataEntity = null
    ){
        if($dataEntity != null){
            $this->postUuid = $dataEntity->postUUID;
            $this->userUuid = $dataEntity->userUUID;
            $this->commentTxt = $dataEntity->commentTxt;
            $this->createdAt = $dataEntity->createdAt;
            return;
        }

    }
}

?>