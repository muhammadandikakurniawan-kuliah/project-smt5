<?php
namespace App\Entities;

use App\Entities\BaseEntity;

class UserFollowEntity extends BaseEntity {

    /** 
    *  @var int 
    */
    public $id;

    /** 
    *  @var string 
    */
    public $userUUID;

    /** 
    *  @var string 
    */
    public $userFollowUUID;

    /**
     * @var AppUserEntity
    */
    public $user;

    public function toArray() : array {
        return [
            "id" => $this->id,
            "user_uuid" => $this->userUUID,
            "user_followed_uuid" => $this->userFollowUUID,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }

}

?>