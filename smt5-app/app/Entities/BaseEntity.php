<?php

namespace App\Entities;
use DateTime;

abstract class BaseEntity {
    /**
     * @var DateTime
     */
    public $createdAt;
    /**
     * @var DateTime
     */
    public $updatedAt;
    /**
     * @var DateTime
     */
    public $deletedAt;
}

?>