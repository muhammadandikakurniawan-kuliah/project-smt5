<?php

namespace App\Entities;

class UserPostFileEntity extends BaseEntity{
    /** 
    * @var int
    */
    public $id;

    /** 
     * @var string
    */
    public $postUuid;

    /** 
     * @var string
    */
    public $filePath;

    /** 
     * @var string
    */
    public $fileName;

    /** 
     * @var string
    */
    public $fileType;

    function __construct(array $dataTable = null){
        if($dataTable == null || count($dataTable) <= 0){
            return;
        }

        $this->id = $dataTable["id"] ?? 0;
        $this->postUuid = $dataTable["post_uuid"] ?? "";
        $this->filePath = $dataTable["file_path"] ?? "";
        $this->fileName = $dataTable["file_name"] ?? "";
        $this->fileType = $dataTable["file_type"] ?? "";
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
    }

    public function toArray() : array {
        return [
            "id" => $this->id,
            "post_uuid" => $this->postUuid,
            "file_path" => $this->filePath,
            "file_name" => $this->fileName,
            "file_type" => $this->fileType,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }
}

?>