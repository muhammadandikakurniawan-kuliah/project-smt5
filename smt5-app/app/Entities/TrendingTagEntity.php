<?php
namespace App\Entities;

use App\Entities\BaseEntity;

class TrendingTagEntity extends BaseEntity {
    /** 
    *  @var string 
    */
    public $tag;

    /**
     * @var int
    */
    public $count;
}

?>