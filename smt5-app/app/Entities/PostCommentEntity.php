<?php
namespace App\Entities;

class PostCommentEntity extends BaseEntity{

    /**
     * @var int
    */
    public $id;

    /**
     * @var string
    */
    public $userUUID = "";

    /**
     * @var string
    */
    public $postUUID = "";

    /**
     * @var string
    */
    public $commentTxt = "";

    /**
     * @var AppUserEntity
    */
    public $user;

    function __construct(array $dataTable = null){
        if($dataTable == null || count($dataTable) <= 0){
            return;
        }

        $this->id = $dataTable["_id"] ?? "";
        $this->userUUID = $dataTable["user_uuid"] ?? [];
        $this->postUUID = $dataTable["post_uuid"] ?? [];
        $this->commentTxt = $dataTable["comment_txt"];
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
    }

    public function toArray() : array {
        return [
            "user_uuid" => $this->userUUID,
            "post_uuid" => $this->postUUID,
            "comment_txt" => $this->commentTxt,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }

}

?>