<?php
namespace App\Entities;

class PostTagEntity extends BaseEntity{

    /**
     * @var string
    */
    public $id;

    /**
     * @var string
    */
    public $postUuid;

    /**
     * @var string[]
    */
    public $tags = [];

    function __construct(array $dataTable = null){
        if($dataTable == null || count($dataTable) <= 0){
            return;
        }

        $this->id = $dataTable["_id"] ?? "";
        $this->tags = $dataTable["tags"] ?? [];
        $this->postUuid = $dataTable["post_uuid"] ?? "";
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
    }

    public function toArray() : array {
        return [
            "post_uuid" => $this->postUuid,
            "tags" => $this->tags,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }

}

?>