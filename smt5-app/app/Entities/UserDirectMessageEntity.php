<?php
namespace App\Entities;

use App\Entities\BaseEntity;

class UserDirectMessageEntity extends BaseEntity {
    /** 
    *  @var int 
    */
    public $id;

    /**
     * @var string
    */
    public $senderUserUUID;

    /**
     * @var string
    */
    public $receiverUserUUID;

    /**
     * @var string
    */
    public $messageTxt;

    /**
     * @var AppUserEntity
    */
    public $sender;

    /**
     * @var AppUserEntity
    */
    public $receiver;

    function __construct(array $dataTable = null){
        if($dataTable == null || empty($dataTable)){
            return;
        }

        $this->id = $dataTable["id"] ?? 0;
        $this->senderUserUUID = $dataTable["sender_user_uuid"] ?? "";
        $this->receiverUserUUID = $dataTable["receiver_user_uuid"] ?? "";
        $this->messageTxt = $dataTable["message_txt"] ?? "";
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
    }

    public function toArray() : array {
        return [
            "id" => $this->id,
            "sender_user_uuid" => $this->senderUserUUID,
            "receiver_user_uuid" => $this->receiverUserUUID,
            "message_txt" => $this->messageTxt,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }
}

?>