<?php

namespace App\Entities;
use DateTime;

class AppUserEntity extends BaseEntity{
    /**
     * @var int 
    */
    public $id = 0;

    /**
     * @var string 
    */
    public $uuid = "";

    /**
     * @var string 
    */
    public $longName = "";

    /**
     * @var string 
    */
    public $username = "";

    /**
     * @var string 
    */
    public $password = "";

    /**
     * @var string 
    */
    public $email = "";

    /**
     * @var string 
    */
    public $phoneNumber = "";

    /**
     * @var ?DateTime
    */
    public $activatedAt;

    /**
     * @var string
    */
    public $profilePictPath;

    /**
     * @var string
    */
    public $coverPictPath;

    function __construct(array $dataTable = null){
        if($dataTable == null || count($dataTable) <= 0){
            return;
        }

        $this->id = $dataTable["id"] ?? 0 ;
        $this->uuid = $dataTable["uuid"] ?? "";
        $this->longName = $dataTable["long_name"] ?? "";
        $this->username = $dataTable["username"] ?? "";
        $this->email = $dataTable["email"] ?? "";
        $this->phoneNumber = $dataTable["phone_number"] ?? "";
        $this->password = $dataTable["password"] ?? "";
        $this->profilePictPath = $dataTable["profile_pict_path"] ?? "";
        $this->coverPictPath = $dataTable["cover_pict_path"] ?? "";
        $this->activatedAt = $dataTable["activated_at"] ?? null;
        $this->createdAt = $dataTable["created_at"] ?? null;
        $this->updatedAt = $dataTable["updated_at"] ?? null;
        $this->deletedAt = $dataTable["deleted_at"] ?? null;
    }

    public function toArray() : array {
        return [
            "id" => $this->id,
            "uuid" => $this->uuid,
            "long_name" => $this->longName,
            "username" => $this->username,
            "password" => $this->password,
            "email" => $this->email,
            "phone_number" => $this->phoneNumber,
            "activated_at" => $this->activatedAt,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
            "profile_pict_path" => $this->profilePictPath,
            "cover_pict_path" => $this->coverPictPath
        ];
    }

}

?>