<?php
namespace App\Entities;

class ContentTagEntity extends BaseEntity{

    /**
     * @var string
    */
    public $id;

    /**
     * @var string
    */
    public $tag = "";

    function __construct(array $dataTable = null){
        if($dataTable == null || count($dataTable) <= 0){
            return;
        }

        $this->id = $dataTable["_id"] ?? "";
        $this->tag = $dataTable["tag"] ?? [];
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
    }

    public function toArray() : array {
        return [
            "tag" => $this->tag,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }

}

?>