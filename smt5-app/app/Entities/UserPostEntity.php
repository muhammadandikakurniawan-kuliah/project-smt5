<?php

namespace App\Entities;

class UserPostEntity extends BaseEntity{
    /** 
    *  @var int 
    */
    public $id;

    /**
     * @var string
    */
    public $uuid;

    /**
     * @var string
    */
    public $userUuid;

    /**
     * @var string
    */
    public $textContent;

    /**
     * @var UserPostFileEntity[]
    */
    public $postFiles = [];

    function __construct(array $dataTable = null){
        if($dataTable == null || empty($dataTable)){
            return;
        }

        $this->id = $dataTable["id"] ?? 0;
        $this->uuid = $dataTable["uuid"] ?? "";
        $this->userUuid = $dataTable["user_uuid"] ?? "";
        $this->textContent = $dataTable["text_content"] ?? "";
        $this->createdAt = $dataTable["created_at"];
        $this->updatedAt = $dataTable["updated_at"];
        $this->deletedAt = $dataTable["deleted_at"];
        $this->postFiles = $dataTable["post_files"] ?? [];
    }

    public function toArray() : array {
        return [
            "id" => $this->id,
            "user_uuid" => $this->userUuid,
            "uuid" => $this->uuid,
            "text_content" => $this->textContent,
            "created_at" => $this->createdAt,
            "updated_at" => $this->updatedAt,
            "deleted_at" => $this->deletedAt,
        ];
    }
}

?>